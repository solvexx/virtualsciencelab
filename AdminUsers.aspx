﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="AdminUsers.aspx.vb" ValidateRequest="false" Inherits="AdminUsers" %>
<%@ Register src="HeaderMenu.ascx" tagname="HeaderMenu" tagprefix="uc1" %>
<%@ Register src="footer.ascx" tagname="footer" tagprefix="uc3" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="ajaxToolkit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Admin Users</title>
    <link href="style.css" rel="stylesheet" type="text/css" />
    <link href="layout.css" rel="stylesheet" type="text/css" />    
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>       
    <script type="text/javascript">
        function setHeartbeat() {
            setTimeout("heartbeat()", 300000); // every 5 min
        }

        function heartbeat() {
            $.post("SessionHeartbeat.ashx");
            setHeartbeat();
        }
	</script>       
</head>
<body onload="setHeartbeat();">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ScriptManager>
    <div id="main">
	    <!--header -->
        <uc1:HeaderMenu ID="HeaderMenu1" runat="server" />
	    <!--header end-->
		<div id="content">
			<div class="left-top">
                <div class="right-top">
                    <div class="left-bot">
                        <div class="right-bot">
                            <div class="bg1">
                                <div class="bg2">
                                	<div class="container">
                                	    <div class="indent">
                                	        <h1>Manage Student Accounts</h1>
                                	        <h2>Import student emails from file</h2>
                                            <p>Student accounts can be imported from a .csv file, which can be easily made with excel as described in this 
                                            <a href="https://support.office.com/en-za/article/Import-or-export-text-txt-or-csv-files-5250ac4c-663c-47ce-937b-339e391393ba" target="_blank">article</a></p><br />
                                	        <asp:FileUpload ID="fileUpload" runat="server" Width="600"/>
                                	        <asp:Button ID="btnImportCSC" runat="server" Text="Import" />
                                	        <asp:Label ID="ErrorUploadMessage" runat="server" CssClass="errormessage" Visible="False"></asp:Label>
                                	        <h2><br />All students</h2>
                                	        <asp:GridView ID="gridUsers" runat="server"
                                                AllowPaging="True"
                                                AllowSorting="False"                
                                                AutoGenerateColumns="False" 
                                                GridLines="None"
                                                PageSize="10" 
                                                ShowFooter="false" 
                                                FooterStyle-CssClass="GridViewFooterStyle"
                                                RowStyle-CssClass="GridViewRowStyle"
                                                SelectedRowStyle-CssClass="GridViewSelectedRowStyle"
                                                PagerStyle-CssClass="GridViewPagerStyle"
                                                AlternatingRowStyle-CssClass="GridViewAlternatingRowStyle"
                                                HeaderStyle-CssClass="GridViewHeaderStyle"
                                                CssClass="GridViewStyle" >
                                                <Columns>
                                                    <asp:BoundField DataField="uID" ReadOnly="true"><HeaderStyle Width="0" />
                                                    </asp:BoundField>
                                                    <asp:TemplateField HeaderText="uID">
                                                        <ItemTemplate>
                                                            <asp:Label ID="userID" runat="server" Text='<%# Bind("uID") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle Width="10%" />
                                                        <FooterStyle Width="10%" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Name">
                                                        <ItemTemplate>
                                                            <asp:Label ID="labName" runat="server" Text='<%# Bind("uName") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle Width="25%" />
                                                        <FooterStyle Width="25%" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="First Name">
                                                        <ItemTemplate>
                                                            <asp:Label ID="labFirstName" runat="server" Text='<%# Bind("uFirstName") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle Width="25%" />
                                                        <FooterStyle Width="25%" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Email">
                                                        <ItemTemplate>
                                                            <asp:Label ID="labEmail" runat="server" Text='<%# Bind("uEmail") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle Width="30%" />
                                                        <FooterStyle Width="30%" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Status">
                                                        <ItemTemplate>
                                                            <asp:Label ID="labEmail" runat="server" Text='<%# Bind("uStatus") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle Width="10%" />
                                                        <FooterStyle Width="10%" />
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                            <h2><br />Send Emails</h2>
                                            <div class="textbox">
                                	            From uID (inclusive): <asp:TextBox ID="txtMin" runat="server" TextAlign="Right">0</asp:TextBox>
                                                To uID (inclusive, leave blank for all): <asp:TextBox ID="txtMax" runat="server"></asp:TextBox>
                                            </div>
                                            <div class="textbox left"><asp:Label ID="Label1" runat="server" Text="Email Subject"></asp:Label>
                                            <asp:TextBox ID="SubjectTextBox" runat="server" Width="834px"></asp:TextBox></div>
                                            <div class="textbox left"><asp:TextBox ID="txtMessage" runat="server" Text='' Width="940px" height="400px" TextMode="MultiLine" Rows="6" CssClass="textbox"></asp:TextBox>
                                            <ajaxToolkit:HtmlEditorExtender ID="txtMessage_HtmlEditorExtender" EnableSanitization="false" 
                                                runat="server" TargetControlID="txtMessage">
                                            </ajaxToolkit:HtmlEditorExtender></div>
                                            <p></p>
                                            <div class="textbox left"><asp:CheckBox ID="chkSendPwd" runat="server" Text="Include password at the end of the email" TextAlign="Right"/></div>
                                            <div class="textbox right"><asp:Button ID="btnSenEmail" runat="server" Text="Send Email(s)" /></div>
                                            <div class="clear"><p></p><asp:Label ID="ErrorEmailMessage" runat="server" CssClass="errormessage" Visible="False"></asp:Label></div>
                                	        <div class="left">Email address to send test email to: <asp:TextBox ID="TestEmail" runat="server" Text='' TextAlign="left"/></div>
                                            <div class="right"><asp:Button ID="btnSendTestEmail" runat="server" Text="Send Test Email" /></div>
                                	    </div>
                                	</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--footer -->
        <uc3:footer ID="footer1" runat="server" />
	    <!--footer end-->
    </div>
    </form>
</body>
</html>
