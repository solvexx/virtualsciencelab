﻿Imports System.Data

Partial Class AdminUsers
    Inherits System.Web.UI.Page
    Dim TAUsers As New AccessDataTableAdapters.UsersTableAdapter
    Dim DTUsers As AccessData.UsersDataTable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim strPageName As String = Right(Page.AppRelativeVirtualPath, Page.AppRelativeVirtualPath.Length - 2)

        'to fix menu pb in Chrome and Safari
        If Request.UserAgent.IndexOf("AppleWebKit") > 0 Then Request.Browser.Adapters.Clear()

        If Request.IsAuthenticated Then
            If Not Page.IsPostBack Then
                'check that the user is of type Admin
                Dim TheUser As New qmulUser
                TheUser.initFromDB(Page.User.Identity.Name)
                If TheUser.uType = qmulUser.userType.Admin Then
                    Track(sender, Context, CommonUtilities.WebTrackingType.View, , User.Identity.Name)
                    updateContents()
                Else
                    'redirect to the Default page
                    Response.Redirect("Default.aspx")
                End If
            End If
        Else
            Response.Redirect("Login.aspx?ReturnURL=" & strPageName)
        End If
    End Sub

    Private Sub updateContents()
        'populate the users grid
        DTUsers = TAUsers.GetAllStudents()

        gridUsers.DataSource = DTUsers
        gridUsers.DataBind()

    End Sub

    Protected Sub gridUsers_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gridUsers.RowCreated
        'hide id columns
        If e.Row.RowType = DataControlRowType.Header Or e.Row.RowType = DataControlRowType.DataRow Or e.Row.RowType = DataControlRowType.Footer Then
            e.Row.Cells(0).Visible = False
        End If
    End Sub

    Protected Sub gridUsers_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gridUsers.PageIndexChanging
        'change the page and rebind the grid
        gridUsers.PageIndex = e.NewPageIndex
        gridUsers.EditIndex = -1
        updateContents()
    End Sub

    Protected Sub btnImportCSC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnImportCSC.Click
        ErrorUploadMessage.Text = ""
        ErrorUploadMessage.Visible = False
        ErrorEmailMessage.Text = ""
        ErrorEmailMessage.Visible = False

        Dim uniqueFileName As String = "ImportStudents"
        'make sure that a file has been selected
        If fileUpload.HasFile Then
            Try
                'save it to the server
                fileUpload.SaveAs(Server.MapPath("~/Admin/") & uniqueFileName)
            Catch ex As Exception
                ErrorUploadMessage.Text = "The file could not be uploaded. The following error occurred: " & ex.Message
                ErrorUploadMessage.Visible = True
            End Try

            Dim reader As System.IO.StreamReader
            Dim totalCSV As Integer = 0
            Dim totalImport As Integer = 0
            Try
                ErrorUploadMessage.Text = ""

                ' open a reader for the input file, and read line by line
                reader = New System.IO.StreamReader(Server.MapPath("~/Admin/") & uniqueFileName)

                Do While reader.Peek() >= 0
                    totalCSV += 1
                    ' read a line and split it into tokens, divided by the specified 
                    ' separators
                    Dim tokens As String() = System.Text.RegularExpressions.Regex.Split _
                        (reader.ReadLine(), ",")
                    'assumes first column name, 2nd first name, last column email
                    'check if already exists
                    DTUsers = TAUsers.GetUser(Trim(tokens(2)))
                    If DTUsers.Rows.Count = 0 Then
                        totalImport += 1
                        TAUsers.Insert(Trim(tokens(0)), Trim(tokens(1)), Trim(tokens(2)), GetRandomPasswordUsingGUID(6), "", 1)
                    End If
                Loop
                ErrorUploadMessage.Text = totalImport & "/" & totalCSV & " students imported"
                updateContents()
            Catch ex As Exception
                ErrorUploadMessage.Text = "Error occured while importing (" & totalImport & "/" & totalCSV & ")"
            Finally
                If Not reader Is Nothing Then reader.Close()
            End Try
        Else
            ErrorUploadMessage.Text = "Please select a file to be uploaded."
        End If
        ErrorUploadMessage.Visible = True
    End Sub

    Private Function GetRandomPasswordUsingGUID(ByVal length As Integer) As String
        'Get the GUID
        Dim guidResult As String = System.Guid.NewGuid().ToString()

        'Remove the hyphens
        guidResult = guidResult.Replace("-", String.Empty)

        'Make sure length is valid
        If length <= 0 OrElse length > guidResult.Length Then
            Throw New ArgumentException("Length must be between 1 and " & guidResult.Length)
        End If

        'Return the first length bytes
        Return guidResult.Substring(0, length)
    End Function

    Protected Sub btnSenEmail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSenEmail.Click, btnSendTestEmail.Click
        ErrorUploadMessage.Text = ""
        ErrorUploadMessage.Visible = False
        ErrorEmailMessage.Text = ""
        ErrorEmailMessage.Visible = False

        'prepare the email
        Dim TAEmailTracking As New AccessDataTableAdapters.EmailTrackingTableAdapter
        Dim rwEmail As AccessData.EmailTrackingRow

        Dim SendingEmail As String = "contact@solvexx.com"
        Dim SendingEmailUserName As String = "contact@solvexx"
        Dim SendingEmailPassword As String = "8a5a9534"
        Dim EmailServer As String = "mail.solvexx.com"

        Dim EMailSubject As String
        If SubjectTextBox.Text = "" Then
            EMailSubject = "3D Virtual Lab"
        Else
            EMailSubject = SubjectTextBox.Text
        End If

        Dim EMailBody As String = ""
        EMailBody = Me.txtMessage.Text
        If (EMailBody = "") Then
            ErrorEmailMessage.Text = "No email was sent"
            ErrorEmailMessage.Visible = True
            Return
        End If
        EMailBody = ReplaceToken(EMailBody, "vbLf", "<br/>")
        If (EMailBody = "") Then EMailBody = Me.txtMessage.Text

        Dim totalActiveStudents As Integer = 0
        Dim totalEmails As Integer = 0

        Dim minUserID As Integer = txtMin.Text
        Dim maxUserID As Integer = Integer.MaxValue
        If txtMax.Text <> "" Then maxUserID = txtMax.Text
        If maxUserID < minUserID Then maxUserID = minUserID

        If (sender.clientid = "btnSenEmail") Then
            DTUsers = TAUsers.GetAllStudents()
            For Each rowUser As AccessData.UsersRow In DTUsers.Rows
                'only send email to active students
                If rowUser.uStatus = "" And rowUser.uID >= minUserID And rowUser.uID <= maxUserID Then
                    totalActiveStudents += 1
                    Dim txtBody As String = EMailBody
                    If chkSendPwd.Checked Then txtBody += rowUser.uPwd
                    'use the common utilities function to send the email, which also logs the email
                    rwEmail = SendEMail(rowUser.uEmail, SendingEmail, EMailSubject, txtBody, SendingEmailUserName, SendingEmailPassword, , rowUser.uID, CommonUtilities.origType.Website, rowUser.uID, EMailSubject, EmailServer)
                    'log the email send result, return the error message in the variable passed by reference
                    If Not rwEmail Is Nothing Then
                        If rwEmail.eResult = "Email Sent" Or rwEmail.eResult = "" Then
                            'successful
                            totalEmails += 1
                        End If
                    End If
                End If
            Next
        Else
            If TestEmail.Text = "" Then
                ErrorEmailMessage.Text = "No email was sent"
                ErrorEmailMessage.Visible = True
            Else
                totalActiveStudents += 1
                If chkSendPwd.Checked Then EMailBody += "retrievedPWD"
                'use the common utilities function to send the email, which also logs the email
                rwEmail = SendEMail(TestEmail.Text, SendingEmail, EMailSubject, EMailBody, SendingEmailUserName, SendingEmailPassword, , 1, CommonUtilities.origType.Website, 1, EMailSubject, EmailServer)
                'log the email send result, return the error message in the variable passed by reference
                If Not rwEmail Is Nothing Then
                    If rwEmail.eResult = "Email Sent" Or rwEmail.eResult = "" Then
                        'successful
                        totalEmails += 1
                    End If
                End If
            End If

        End If

        ErrorEmailMessage.Text = totalEmails & "/" & totalActiveStudents & " were sent"
        ErrorEmailMessage.Visible = True

    End Sub
End Class
