﻿Imports Microsoft.VisualBasic
Imports System.Collections.ObjectModel

Public Class NewsItem
    Dim TANews As New AdminDataTableAdapters.NewsTableAdapter
    Dim DTNews As AdminData.AdminDataTable

    Private _newsID As Integer
    Private _News As String
    Private _Headline As String
    Private _CreationDate As Date
    Private _TheOnlyOne As Boolean

    Public Property newsID() As Integer
        Get
            Return _newsID
        End Get
        Set(ByVal value As Integer)
            _newsID = value
        End Set
    End Property

    Public Property News() As String
        Get
            Return _News
        End Get
        Set(ByVal value As String)
            _News = value
        End Set
    End Property

    Public Property Headline() As String
        Get
            Return _Headline
        End Get
        Set(ByVal value As String)
            _Headline = value
        End Set
    End Property

    Public Property CreationDate() As Date
        Get
            Return _CreationDate
        End Get
        Set(ByVal value As Date)
            _CreationDate = value
        End Set
    End Property

    Public Property TheOnlyOne() As Boolean
        Get
            Return _TheOnlyOne
        End Get
        Set(ByVal value As Boolean)
            _TheOnlyOne = value
        End Set
    End Property

    Public Sub initFromDB(Optional ByVal nID As Integer = 0)
        If nID > 0 Then
            'return the specified one
            DTNews = TANews.GetNewsByID(nID)
        Else
            'return the latest one
            DTNews = TANews.GetLatestNews
        End If

        If DTNews.Rows.Count > 0 Then
            'a record was found set the properties accordingly
            _newsID = DTNews(0).newsID
            _News = DTNews(0).News
            _Headline = DTNews(0).Headline
            _CreationDate = DTNews(0).creationDate
            If nID = 0 Then
                'in case of the latest one assess if the only one
                _TheOnlyOne = True
                'retrieve all news in reverse chronological order (part of TA definition)
                DTNews = TANews.GetNews
                If DTNews.Rows.Count > 1 Then _TheOnlyOne = False
            End If
        End If
    End Sub

    Public Sub saveToDb()
        If _newsID = 0 Then
            'creates a new record
            TANews.Insert(Now(), _Headline, _News)
        Else
            TANews.UpdateNews(_Headline, _News, _newsID)
        End If
    End Sub

    Public Sub deleteFromDB()
        TANews.Delete(_newsID)
    End Sub

End Class

Public Class AllNews
    Private _newsItems As Collection(Of NewsItem)

    Public Property newsItems() As Collection(Of NewsItem)
        Get
            Return _newsItems
        End Get
        Set(ByVal value As Collection(Of NewsItem))
            _newsItems = value
        End Set
    End Property

    Public Sub initFromDB()
        _newsItems = New Collection(Of NewsItem)

        'retrieve all news in reverse chronological order (part of TA definition)
        Dim TANews As New AdminDataTableAdapters.NewsTableAdapter
        Dim DTNews As AdminData.AdminDataTable = TANews.GetNews

        For Each rowNews As AdminData.NewsRow In DTNews
            Dim TheNews As New NewsItem
            TheNews.newsID = rowNews.newsID
            TheNews.News = rowNews.News
            TheNews.Headline = rowNews.Headline
            TheNews.CreationDate = rowNews.creationDate
            _newsItems.Add(TheNews)
        Next
    End Sub
End Class

Public Class Feedback
    Private _userID As Integer
    Private _uAssessmentID As Integer
    Private _Student As String
    Private _Feedback As String
    Private _Mark As Double

    Public Property userID() As Integer
        Get
            Return _userID
        End Get
        Set(ByVal value As Integer)
            _userID = value
        End Set
    End Property

    Public Property uAssessmentID() As Integer
        Get
            Return _uAssessmentID
        End Get
        Set(ByVal value As Integer)
            _uAssessmentID = value
        End Set
    End Property

    Public Property Student() As String
        Get
            Return _Student
        End Get
        Set(ByVal value As String)
            _Student = value
        End Set
    End Property

    Public Property Feedback() As String
        Get
            Return _Feedback
        End Get
        Set(ByVal value As String)
            _Feedback = value
        End Set
    End Property

    Public Property Mark() As Double
        Get
            Return _Mark
        End Get
        Set(ByVal value As Double)
            _Mark = value
        End Set
    End Property

    'Public Sub initFromDB(ByVal uaID As Integer, ByVal aType As Assessment.assessmentType)
    '    Dim TAFeedbacks As New AdminDataTableAdapters.FeedbacksTableAdapter
    '    Dim DTFeedbacks As AdminData.FeedbacksDataTable = TAFeedbacks.GetAssessmentByID(uaID)
    '    If DTFeedbacks.Rows.Count > 0 Then
    '        _userID = DTFeedbacks(0).uID
    '        _uAssessmentID = DTFeedbacks(0).uaID
    '        _Student = DTFeedbacks(0).uFirstName & " " & DTFeedbacks(0).uName
    '        _Feedback = DTFeedbacks(0).feedback
    '        _Mark = DTFeedbacks(0).mark
    '    End If
    'End Sub

    Public Sub saveToDB()
        Dim TAAssessments As New AdminDataTableAdapters.UserAssessmentsTableAdapter
        TAAssessments.Update(_Mark, _Feedback, _uAssessmentID)
    End Sub
End Class

Public Class AllFeedbacks
    Private _feedbackItems As Collection(Of Feedback)

    Public Property feedbackItems() As Collection(Of Feedback)
        Get
            Return _feedbackItems
        End Get
        Set(ByVal value As Collection(Of Feedback))
            _feedbackItems = value
        End Set
    End Property

    'Public Sub initFromDB(ByVal aType As Assessment.assessmentType)
    '    _feedbackItems = New Collection(Of Feedback)

    '    Dim TAFeedbacks As New AdminDataTableAdapters.FeedbacksTableAdapter
    '    Dim DTFeedbacks As AdminData.FeedbacksDataTable = TAFeedbacks.GetAssessmentsByType(aType)
    '    For Each rowFeedback As AdminData.FeedbacksRow In DTFeedbacks
    '        Dim TheFeedback As New Feedback
    '        TheFeedback.userID = rowFeedback.uID
    '        TheFeedback.uAssessmentID = rowFeedback.uaID
    '        TheFeedback.Student = rowFeedback.uFirstName & " " & rowFeedback.uName
    '        TheFeedback.Feedback = rowFeedback.feedback
    '        TheFeedback.Mark = rowFeedback.mark
    '        _feedbackItems.Add(TheFeedback)
    '    Next
    'End Sub
End Class

Public Class qmulUser
    Public Enum userType
        'need to be the same as the unique ID in the DB
        Student = 1
        Admin = 2
        Guest = 3
        Dummy = 4
        Staff = 5
    End Enum

    Private _uID As Integer
    Private _firstName As String
    Private _Name As String
    Private _DepartmentDesc As String
    Private _OrganisationDesc As String
    Private _uType As userType

    Public Property uID() As Integer
        Get
            Return _uID
        End Get
        Set(ByVal value As Integer)
            _uID = value
        End Set
    End Property

    Public Property firstName() As String
        Get
            Return _firstName
        End Get
        Set(ByVal value As String)
            _firstName = value
        End Set
    End Property

    Public Property Name() As String
        Get
            Return _Name
        End Get
        Set(ByVal value As String)
            _Name = value
        End Set
    End Property

    Public Property uType() As userType
        Get
            Return _uType
        End Get
        Set(ByVal value As userType)
            _uType = value
        End Set
    End Property

    Public Property OrganisationDesc() As String
        Get
            Return _OrganisationDesc
        End Get
        Set(ByVal value As String)
            _OrganisationDesc = value
        End Set
    End Property

    Public Property DepartmentDesc() As String
        Get
            Return _DepartmentDesc
        End Get
        Set(ByVal value As String)
            _DepartmentDesc = value
        End Set
    End Property

    Public Sub initFromDB(ByVal userID As Integer)
        'store the userID
        _uID = userID
        Dim TAUsers As New AccessDataTableAdapters.UsersTableAdapter
        Dim DTUsers As AccessData.UsersDataTable = TAUsers.GetUserByID(_uID)
        If DTUsers.Rows.Count > 0 Then
            _firstName = DTUsers(0).uFirstName
            _Name = DTUsers(0).uName
            _uType = DTUsers(0).uType
        End If

        'get the department for the user
        Dim TAUserDepartments As New AccessDataTableAdapters.UserDepartmentsTableAdapter
        Dim DTUserDepartments As AccessData.UserDepartmentsDataTable = TAUserDepartments.GetDepartmentForUser(_uID)
        If DTUserDepartments.Count > 0 Then
            Dim TADepartments As New AccessDataTableAdapters.DepartmentTableAdapter
            'only display the first department
            Dim DTDepartments As AccessData.DepartmentDataTable = TADepartments.GetDepartment(DTUserDepartments(0).depID)
            If DTDepartments.Count > 0 Then
                _DepartmentDesc = DTDepartments(0).Description
                Dim TAOrgs As New AccessDataTableAdapters.OrganisationTableAdapter
                Dim DTOrgs As AccessData.OrganisationDataTable = TAOrgs.GetOrganisation(DTDepartments(0).OrgID)
                If DTOrgs.Count > 0 Then
                    _OrganisationDesc = DTOrgs(0).OrgName
                End If
            End If
        End If
    End Sub
End Class

'Base class for pages with popups, so popups can be called from classes and modules external to the pages.
Public Class PagesWithPopups
    Inherits System.Web.UI.Page

    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        MyBase.OnLoad(e)
    End Sub

    Public Overridable Sub showAPopup(ByVal thePage As PagesWithPopups, ByVal strMessage As String, Optional ByVal popupID As Integer = 0)

    End Sub
End Class

