﻿Imports Microsoft.VisualBasic

Public Module AdminUtilities
    'Public Function substractHints(ByVal uID As Integer, ByVal origMark As Double) As Double
    '    Dim finalMark As Double = origMark

    '    'retrieve the selfAssessment
    '    Dim TheSelfAssessment As New Assessment
    '    TheSelfAssessment.initFromDB(Assessment.assessmentType.SelfAssessment, uID)
    '    'substract from the mark the hints accessed by the user during the self assessment
    '    finalMark = finalMark - addScoreImpact(TheSelfAssessment)

    '    'retrieve the 2 peers
    '    Dim TheGroup As New PeerGroup
    '    TheGroup.initFromDB(0, uID)
    '    Dim ThePeerReview As Assessment
    '    For Each ThePeer As Integer In TheGroup.peers
    '        If ThePeer <> uID Then
    '            'retrieve the peer review
    '            ThePeerReview = New Assessment
    '            ThePeerReview.initFromDB(Assessment.assessmentType.PeerReview, uID, ThePeer)
    '            'substract from the mark the hints accessed by the user during the peer review
    '            finalMark = finalMark - addScoreImpact(ThePeerReview)
    '        End If
    '    Next

    '    Return finalMark
    'End Function

    'Private Function addScoreImpact(ByVal TheAssessment As Assessment) As Double
    '    'returns the sum of all hints accessed by the user for all questions part of the specified assessment
    '    Dim impact As Double = 0
    '    'Go through all questions
    '    For Each TheQuestion As Question In TheAssessment.aQuestions
    '        'for each questions go through all hints
    '        For Each TheHint As Hint In TheQuestion.qHints
    '            If TheHint.accessedByUser Then
    '                'the user has accessed the hint, ie acknowlodge the impact
    '                impact = impact + TheHint.scoreImpact
    '            Else
    '                'the hints of a questions are ordered 
    '                'therefore as soon as the first hint the user has NOT accessed is encountered
    '                'no need to look further for that question
    '                Exit For
    '            End If
    '        Next
    '    Next
    '    Return impact
    'End Function
End Module
