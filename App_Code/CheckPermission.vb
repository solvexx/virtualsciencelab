﻿Imports Microsoft.VisualBasic

Public Module Utilities
    Public Function PreCond(ByVal uID As Integer, Optional ByVal strPageName As String = "", Optional ByVal pID As Integer = 0) As Boolean
        'Returns True if a page can be loaded
        PreCond = True
        'retrieves the page definition
        Dim pageID As Integer
        Dim TAPages As New AccessDataTableAdapters.PagesTableAdapter
        Dim DTPages As AccessData.PagesDataTable
        If pID = 0 Then
            'no unique page ID was specified use the page name to retrieve the page definition
            DTPages = TAPages.GetPageByName(strPageName)
        Else
            'the unique page ID was specified, use it to retrieve the page definition
            DTPages = TAPages.GetPageByID(pID)
        End If

        If DTPages.Rows.Count > 0 Then
            'page found
            pageID = DTPages(0).pageID
            'check if permission required
            If DTPages(0).checkPermission Then
                Dim TAPermission As New AccessDataTableAdapters.UserPagePermissionsTableAdapter
                Dim DTPermission As AccessData.UserPagePermissionsDataTable = TAPermission.GetPermissionByPageID(uID, pageID)
                If DTPermission.Rows.Count = 0 Then
                    'that page is currently not listed, check if precondition is met
                    Dim boolPreCondMet As Boolean = True
                    'retrieve the preconditions
                    Dim TAPreConds As New AccessDataTableAdapters.PagePreConditionsTableAdapter
                    Dim DTPreConds As AccessData.PagePreConditionsDataTable = TAPreConds.GetPagePreConditions(pageID)
                    Dim TAUserTracking As New AccessDataTableAdapters.UserTrackingTableAdapter
                    Dim DTUserTracking As AccessData.UserTrackingDataTable
                    For Each rowPreCond As AccessData.PagePreConditionsRow In DTPreConds.Rows
                        'loops through all preconditions for that page
                        'All preconditions need to be met for boolPreCondMet to stay at True
                        If Not rowPreCond.gotoQuiz Then
                            If rowPreCond.chapID = -1 Then
                                'precondition based on a learnexx3D embed ie UNITY
                                'check the procedure was submitted
                                DTUserTracking = TAUserTracking.GetProcSubmitByUser(uID, rowPreCond.procID)
                                If DTUserTracking.Rows.Count = 0 Then
                                    'the user has not yet successfully submitted that procedure
                                    boolPreCondMet = False
                                End If
                            End If
                        End If
                    Next
                    If Not boolPreCondMet Or DTPreConds.Rows.Count = 0 Then
                        'at least one preconditions was not met
                        PreCond = False
                    Else
                        'all preconditions were met therefore
                        'creates the permission record which is used by menu.ascx.vb
                        TAPermission.Insert(uID, pageID, True)
                    End If
                ElseIf Not DTPermission(0).active Then
                    'permission no more valid
                    PreCond = False
                End If
            Else
                'no permission required therefore no precondition to check
            End If
        End If
    End Function

    Public Function beforeValidityEndDate(ByVal strPageName As String, Optional ByVal deltaDays As Integer = 0) As Boolean
        'used by SelfAssessment and PeerReview to check that one can still submit
        'the start validity of the next page is the end of validity of the calling page minus the specified delta (IN DAYS)
        'retrieve the page
        Dim TAPages As New AccessDataTableAdapters.PagesTableAdapter
        Dim DTPages As AccessData.PagesDataTable = TAPages.GetPageByName(strPageName)
        If DTPages.Rows.Count > 0 Then
            If DateAdd(DateInterval.Day, -deltaDays, DTPages(0).validFrom) < Now Then
                'the next page is now valid therefore the current one is no more
                Return False
            Else
                'the next page is not valid yet therefore still possible to submit
                Return True
            End If
        Else
            'should never happen
            Return False
        End If
    End Function
End Module
