﻿Namespace CommonUtilities

    Public Enum origType
        Campaign
        Website
    End Enum

    Public Enum WebTrackingType
        ClickTrough
        View
        Email
        Login
    End Enum

End Namespace