﻿Imports Microsoft.VisualBasic
Imports System.Xml
Imports System.Xml.Serialization

Public Class ProcedureRunData
    'list of equipment to be instantiated
    'and their contents, eg assembled children, contained materials
    <XmlArrayAttribute("Items")> _
    Public theItems As EqMatBuildData()
End Class


Public Class EqMatBuildData
    'which specialisation of EqMatBuild was used to save the xml data
    'and which shall be used to load it
    <XmlAttribute()> _
    Public EqMatBuildName As String = ""

    'used in the case where there is a configurable builder to handle a common type e.g. beakers of different sizes
    'sets which specific type of the eqpt to build, eg 400ml beaker or 200ml beaker
    'for non configurable builders it is left to null and therefore is not included in the xml data
    <XmlElementAttribute(IsNullable:=False)> _
    Public EqMatBuildType As String = Nothing
End Class
