Imports System.Net.Sockets
Imports CommonUtilities

Public Module WebTrackingUtilities

    Public Sub Track(ByVal sender As Object, ByVal context As System.Web.HttpContext, _
        ByVal Type As CommonUtilities.WebTrackingType, Optional ByVal ClickTarget As String = "", _
        Optional ByVal UserID As Integer = 0, Optional ByVal origType As CommonUtilities.origType = origType.Website, _
        Optional ByVal origID As Integer = 0)
        'Stores tracking information

        Dim TAWebTracking As New AccessDataTableAdapters.WebTrackingTableAdapter

        Dim Request As System.Web.HttpRequest = sender.page.request
        Dim Session As System.Web.SessionState.HttpSessionState = sender.page.session
        Dim source = sender.clientid


        'Store the Client Browser Type
        Dim strBrowser As String = ""
        If Not Request.Browser.Browser Is Nothing Then
            strBrowser = "" & Request.Browser.Browser.ToString
        End If


        'The IP address that the user requested the page from
        Dim strHostAddr As String = ""
        If Not Request.UserHostName Is Nothing Then
            strHostAddr = "" & Request.UserHostName.ToString
        End If

        'Check to see if a DNS lookup for this session has been done already and stored as a session variable and if so use it
        'This is for performance as DNS lookups which cannot be resolved are very time consuming and are therefore only done once
        Dim strHostName As String = ""
        'TO BE DONE ASYNCHRO AS TO TIME CONSUMING
        'If Session("SessionIdent") = Session.SessionID Then
        '    strHostName = Session("DNSResult")
        'Else
        '    'Do a DNS call to look up the HostName of the IP address the user requested the page from
        '    Try
        '        Dim objName As System.Net.IPHostEntry = System.Net.Dns.GetHostEntry(strHostAddr)
        '        strHostName = "" & objName.HostName
        '        Session("DNSResult") = strHostName
        '    Catch e As SocketException
        '    Catch e As FormatException
        '    Catch e As ArgumentNullException
        '    Catch e As Exception
        '    End Try

        'End If

        'Store the .NET session ID of the user
        Dim strSessionID As String = ""
        If Not Session.SessionID Is Nothing Then
            strSessionID = "" & Session.SessionID
            Session("SessionIdent") = strSessionID
        End If

        'The current URL the view or click was shown on
        Dim strURL As String = ""
        If Not Request.Url Is Nothing Then
            'only track the webserver name eg http:\\ogi.hospitalityservices.biz
            'thus truncate the page name and possible query string
            strURL = Request.Url.GetLeftPart(UriPartial.Authority).ToLower
        End If

        'Referring URL
        Dim strURLReferrer As String = ""
        If Not Request.UrlReferrer Is Nothing Then
            If Len(Request.Url.ToString) - Len(Request.Url.PathAndQuery.ToString) >= 0 And Len(Request.UrlReferrer.ToString) - Len(Request.UrlReferrer.PathAndQuery.ToString) >= 0 Then
                If Left(Request.Url.ToString, Len(Request.Url.ToString) - Len(Request.Url.PathAndQuery.ToString)).ToLower = _
                Left(Request.UrlReferrer.ToString, Len(Request.UrlReferrer.ToString) - Len(Request.UrlReferrer.PathAndQuery.ToString)).ToLower Then
                    'in case called within the same subdomain only track the page from which was called though not in case of click through
                    If Type = WebTrackingType.View Then strURLReferrer = Request.Path
                Else
                    strURLReferrer = "" & Request.UrlReferrer.ToString.ToLower
                End If
            Else
                strURLReferrer = "" & Request.UrlReferrer.ToString.ToLower
            End If
        End If

        strURLReferrer = Left(strURLReferrer, 200)

        'The name of the page that the control is on
        Dim strPage As String = ""
        If Not Request.CurrentExecutionFilePath Is Nothing Then
            strPage = Request.CurrentExecutionFilePath
        End If

        TAWebTracking.Insert(Now, strURL, strPage, source, Type, strSessionID, strBrowser, strURLReferrer, _
                ClickTarget, strHostAddr, strHostName, IIf(UserID = 0, Nothing, UserID), origType, origID)
    End Sub

End Module
