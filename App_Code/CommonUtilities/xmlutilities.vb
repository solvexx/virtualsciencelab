﻿Imports Microsoft.VisualBasic
Imports System.Xml
Imports System.Xml.Serialization
Imports System.IO
Imports System

Public Class EqList
    Public BuilderName As String = ""
    Public BuilderType As String = ""
End Class
Public Module xmlutilities

    Public Function LoadFromXml(xmlFileName As String) As List(Of EqList)
        'returns a list of unique builderName and Type

        ' Creates an instance of the XmlSerializer class;
        ' specifies the type of object to be deserialized.
        Dim serializer As New XmlSerializer(GetType(ProcedureRunData))

        ' A FileStream is needed to read the XML document.
        Dim fs As FileStream = Nothing
        Dim resourceString As String = ""
        Try
            fs = New FileStream(xmlFileName, FileMode.Open)
        Catch
        End Try

        ' Declares an object variable of the type to be deserialized.
        Dim theData As ProcedureRunData = Nothing

        ' Uses the Deserialize method to restore the object's state 
        ' with data from the XML document. */
        If fs IsNot Nothing Then
            theData = DirectCast(serializer.Deserialize(fs), ProcedureRunData)
            fs.Dispose()
        ElseIf resourceString <> "" Then
            Dim stringReader As New StringReader(resourceString)
            Dim xmlReader As New XmlTextReader(stringReader)
            theData = DirectCast(serializer.Deserialize(xmlReader), ProcedureRunData)
            stringReader.Close()
            xmlReader.Close()
        End If

        Dim theEqMatBuilds As New List(Of EqList)()

        If theData IsNot Nothing Then
            For Each theItem As EqMatBuildData In theData.theItems
                Dim theEqList As EqList = New EqList()
                theEqList.BuilderName = theItem.EqMatBuildName
                theEqList.BuilderType = theItem.EqMatBuildType
                If (theEqMatBuilds.Find(Function(x) x.BuilderName = theEqList.BuilderName And x.BuilderType = theEqList.BuilderType) Is Nothing) Then
                    theEqMatBuilds.Add(theEqList)
                End If
            Next
        End If

        Return theEqMatBuilds
    End Function

End Module
