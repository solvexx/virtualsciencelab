﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="TextPage.aspx.vb" Inherits="TextPage"%>
<%@ Register src="HeaderMenu.ascx" tagname="HeaderMenu" tagprefix="uc1" %>
<%@ Register src="footer.ascx" tagname="footer" tagprefix="uc3" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Home</title>
    <link href="style.css" rel="stylesheet" type="text/css" />
    <link href="layout.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>       
    <script type="text/javascript">
        function setHeartbeat() {
            setTimeout("heartbeat()", 300000); // every 5 min
        }

        function heartbeat() {
            $.post("SessionHeartbeat.ashx");
            setHeartbeat();
        }
	</script>       
</head>
<body onload="setHeartbeat();">
    <form id="form1" runat="server">
    <div id="main">
	    <!--header -->
        <uc1:HeaderMenu ID="HeaderMenu1" runat="server" />
	    <!--header end-->
	    <div id="content">
		    <div class="left-top">
                <div class="right-top">
                    <div class="left-bot">
                        <div class="right-bot">
                            <div class="bg1">
                                <div class="bg2">
                            	    <div class="container">
                            	        <div class="indent">
                            	            <h1>Virtual Science Lab</h1>
                            	            Learn lab techniques, get familiar with equipment and practise lab methods.<br /><br>      
                                            Here's a quick run through of the tutorial showing the controls and the types of things you can do:     
                                            <div class="padding1" style="text-align:center"><iframe width="420" height="315" src="//www.youtube.com/embed/PrgToeYzR4E" frameborder="0" allowfullscreen></iframe></div>                 	                                        	            
                            	            <h2 class="padding2">Getting Started</h2>
                                            <div class="padding1">Please do the <a href="VirtualLabTutorial.aspx" target="_blank">tutorial</a> first! It only takes a few minutes and whilst the basic controls of the lab are fairly obvious, 
                            	            you also need to be aware of the more advanced ones, like how to pick something up if you drop it on the floor or to hold something in your other hand.</div>
                                            <div class="padding1">You need to use Firefox, Safari or Internet Explorer to use the 3D virtual lab. It does not work on Chrome or Edge. The labs open in their own tabs so you might need to allow popups for this site.</div>
                                            <div class="padding1">The best way to use the lab is with a mouse and a keyboard but everything can also be done using just a keyboard. A laptop mousepad is not ideal. </div>
                            	            </div>
                            	    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
	    </div>
	    <!--footer -->
         <uc3:footer ID="footer1" runat="server" />
	    <!--footer end-->
	</div>	
    </form>
</body>
</html>
