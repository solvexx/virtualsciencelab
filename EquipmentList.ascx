﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="EquipmentList.ascx.vb" Inherits="EquipmentList" %>
<h2 class="clear">Equipment List</h2>
<asp:GridView ID="gridEquip" runat="server"
                AllowPaging="True"
                AllowSorting="true"                
                AutoGenerateColumns="False" 
                GridLines="None"
                PageSize="30" 
                ShowFooter="False" 
                ShowHeader="false"
                FooterStyle-CssClass="GridViewFooterStyle"
                RowStyle-CssClass="GridViewRowStyle"
                SelectedRowStyle-CssClass="GridViewSelectedRowStyle"
                PagerStyle-CssClass="GridViewPagerStyle"
                AlternatingRowStyle-CssClass="GridViewAlternatingRowStyle"
                HeaderStyle-CssClass="GridViewHeaderStyle"
                CssClass="GridViewStyle"
                DataKeyNames="Name,Type"
                                                                
    >
<Columns>
    <asp:TemplateField>
        <ItemTemplate>
            <asp:Label ID="Desc" runat="server" Text='<%# Bind("Desc") %>'></asp:Label>
            <asp:ImageButton ID="btnMoreInfo" runat="server" CausesValidation="false" OnCommand="ShowMoreInfo" CommandName="ShowMoreInfo" ImageUrl="~/images/btnMoreInfoSmall.jpg" AlternateText="More Info" />
        </ItemTemplate>
        <HeaderStyle Width="70%" />
    </asp:TemplateField>
        <asp:TemplateField>
        <ItemTemplate>
            <asp:Image ID="Image1" ImageUrl='<%# Bind("Image") %>' runat="server" /> 
        </ItemTemplate>
        <HeaderStyle Width="30%" />
    </asp:TemplateField>                                                                                                                                                                                          
</Columns>
</asp:GridView>