﻿Imports System.IO
Imports System.Data

Partial Class EquipmentList
    Inherits System.Web.UI.UserControl

    Dim TAEquip As New AccessDataTableAdapters.EquipmentLinksTableAdapter
    Dim labEqtList As List(Of EqList)

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Dim rootPath As String = Server.MapPath("~")
        labEqtList = New List(Of EqList)()
        'read hiddenfield on the page it's on to get the name of the xml file it should read for list of equipment to include.
        Dim theHiddenField As HiddenField = Me.Parent.FindControl("hiddenxmlfilename")
        labEqtList = xmlutilities.LoadFromXml(rootPath + theHiddenField.Value)
        updateContents()
    End Sub

    'setup datatable with equipment descriptions and populate the gridview
    Private Sub updateContents(Optional ByVal sortExp As String = "")
        If sortExp <> "" Then ViewState("gridEquipSort") = sortExp
        Dim DTEquipment As New DataTable
        DTEquipment = getEquipment()

        Dim DVEquipment As DataView = New DataView(DTEquipment)
        DVEquipment.Sort = ViewState("gridEquipSort")
        gridEquip.DataSource = DVEquipment
        gridEquip.DataBind()
    End Sub

    'get the descriptions from the database corresponding to the equipment list in the xml file and return them in a datatable ready for binding to the gridview
    Private Function getEquipment() As DataTable
        Dim DTEquip As New DataTable
        DTEquip.Columns.Add("Name")
        DTEquip.Columns.Add("Type")
        DTEquip.Columns.Add("Desc")
        DTEquip.Columns.Add("Image")
        DTEquip.Columns.Add("Link")

        Dim DTRawEq As AccessData.EquipmentLinksDataTable
        DTRawEq = TAEquip.GetEquipment()
        Dim RWRawEq() As AccessData.EquipmentLinksRow
        Dim BuilderName As String = ""
        For Each eqItem As EqList In labEqtList
            'remove virtuallab2server.
            BuilderName = Strings.Right(eqItem.BuilderName, eqItem.BuilderName.Length - "virtuallab2server.".Length)
            Try
                RWRawEq = DTRawEq.Select("Name = '" + BuilderName + "' AND Type = '" + eqItem.BuilderType + "'")
                If (RWRawEq.Count > 0) Then
                    DTEquip.Rows.Add(eqItem.BuilderName, eqItem.BuilderType, "<b>" + RWRawEq(0).DisplayName + "</b><br /><br />" + RWRawEq(0).Description, RWRawEq(0).ImageLink, RWRawEq(0).URL)
                End If
            Catch ex As Exception

            End Try
        Next

        Return DTEquip
    End Function

    'handles more info button press in the gridview which then links to an external page from the equipment manufacturer
    'should open a new browser window, but loads over the top for now
    Protected Sub ShowMoreInfo(ByVal sender As Object, ByVal e As CommandEventArgs)
        Dim btnImage As System.Web.UI.WebControls.ImageButton = sender
        Dim selRow As System.Web.UI.WebControls.GridViewRow = btnImage.Parent.NamingContainer
        Dim Name As String = gridEquip.DataKeys(selRow.RowIndex).Values("Name").ToString()
        Name = Strings.Right(Name, Name.Length - "virtuallab2server.".Length)
        Dim Type As String = gridEquip.DataKeys(selRow.RowIndex).Values("Type").ToString()
        Dim DTEquipment As AccessData.EquipmentLinksDataTable = TAEquip.GetEquipmentItem(Name, Type)
        'CommonUtilities.ResponseHelper.Redirect(DTEquipment(0).URL, "_blank", "menubar=0,scrollbars=1,width=780,height=900,top=10")))
        Response.Redirect(DTEquipment(0).URL)
    End Sub


End Class
