﻿Imports System.Security.Cryptography
Imports System.Threading

Partial Class _Error
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim delay As Byte() = New Byte(0) {}
            Dim prng As RandomNumberGenerator = New RNGCryptoServiceProvider()

            prng.GetBytes(delay)
            Thread.Sleep(CType(delay(0), Integer))

            Dim disposable As IDisposable = TryCast(prng, IDisposable)
            If Not disposable Is Nothing Then
                disposable.Dispose()
            End If
        End If
    End Sub

    Protected Sub btnResume_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnResume.Click
        Response.Redirect("Default.aspx")
    End Sub
End Class
