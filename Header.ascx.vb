﻿Imports System.Globalization
Imports System.Threading
Imports System.Data

Partial Class Header
    Inherits System.Web.UI.UserControl

    Protected Sub btnLogout_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLogout.Click
        If btnLogout.ImageUrl = "images/btnLogout.jpg" Then
            FormsAuthentication.SignOut()
            'required to make sure that if logs in as somebody else, the support user is re-processed
            Session.Abandon()
            Response.Redirect("Login.aspx")
        ElseIf btnLogout.ImageUrl = "images/btnClose.jpg" Then
            Dim strClose As String
            strClose = "<script>javascript:window.close();</script>"
            Page.Controls.Add(New LiteralControl(strClose))
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'hide the logout button
        Select Case Page.AppRelativeVirtualPath.ToLower
            Case "~/login.aspx", "~/error.aspx"
                btnLogout.Visible = False
            Case "~/news.aspx", "~/peeranalysis.aspx", "~/adminfeedbackdata.aspx", "~/survey.aspx"
                btnLogout.ImageUrl = "images/btnClose.jpg"
                btnLogout.Visible = True
            Case Else
                btnLogout.ImageUrl = "images/btnLogout.jpg"
                btnLogout.Visible = True
        End Select
        If (Page.FindControl("menu1") Is Nothing) Then
            btnLogout.Visible = False
        End If

    End Sub
End Class
