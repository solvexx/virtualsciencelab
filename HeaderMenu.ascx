﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="HeaderMenu.ascx.vb" Inherits="HeaderMenu" %>
	    <!--header -->
	    <div id="header">
        <div class="orgpackagestyle"><asp:Label ID="UniPackageLabel" runat="server" Text=""></asp:Label>
            </div>
	        <div id="topBg">
                <div class="menuRow">
                    <div class="headerstyle">
                        <asp:ImageButton ID="btnLogout" runat="server" ImageUrl="images/btnLogout.jpg" alt="Logout" CausesValidation="False"/>
                    </div>
                    <asp:Menu ID="menu1" runat="server" Orientation="Horizontal" StaticEnableDefaultPopOutImage="false" 
                        DynamicVerticalOffset="4" DynamicEnableDefaultPopOutImage="false" MaximumDynamicDisplayLevels="1"
                        >
                        <Items>
                            <asp:MenuItem Text="Home" Selectable="true" Value="0" >
                            </asp:MenuItem>
                            <asp:MenuItem Text="How To" Selectable="true" Value="4" >
                            </asp:MenuItem>
                            <asp:MenuItem Text="Tutorial" Selectable="true" Value="1" target="_blank">
                            </asp:MenuItem>
                            <asp:MenuItem Text="Techniques" Selectable="false" Value="2" >
                                <asp:MenuItem Text="How to use an analytical balance" Value="20"></asp:MenuItem>
                                <asp:MenuItem Text="Eppendorf 5340 - Balance a centrifuge" Value="21"></asp:MenuItem>
                                <asp:MenuItem Text="Biowave II - Measure absorbance" Value="22"></asp:MenuItem>
                                <asp:MenuItem Text="How to set up pipettes" Value="23"></asp:MenuItem>
                            </asp:MenuItem> 
                            <asp:MenuItem Text="Methods" Selectable="false" Value="3" >        
                                <asp:MenuItem Text="Ethanoic acid titration" Value="30" ></asp:MenuItem>
                                <asp:MenuItem Text="Glycine hydrochloride acid titration" Value="31" ></asp:MenuItem>
                                <asp:MenuItem Text="Strong acid - strong base titration" Value="32" ></asp:MenuItem>
                                <asp:MenuItem Text="Weak acid - strong base titration" Value="33" ></asp:MenuItem>
                                <asp:MenuItem Text="Ethanoic acid / ethanoate buffer solutions" Value="34" ></asp:MenuItem>
                                <asp:MenuItem Text="Inquiry based buffer solution preparation" Value="35" ></asp:MenuItem>
                                <asp:MenuItem Text="Determination of Iron by spectrophotometry" Value="36" ></asp:MenuItem>
                                <asp:MenuItem Text="Cell counting" Value="37" ></asp:MenuItem>
                                <asp:MenuItem Text="Bioreactor mechanical loading" Value="38" ></asp:MenuItem>
                                <asp:MenuItem Text="sGAG assay" Value="39" ></asp:MenuItem>
                                <asp:MenuItem Text="sGAG assay (short)" Value="40" ></asp:MenuItem>
                            </asp:MenuItem>
                            <asp:MenuItem Text="Account" Selectable="False" Value="7" >
                                <asp:MenuItem Text="Packages" Value="70" ></asp:MenuItem>
                            </asp:MenuItem> 
                            <asp:MenuItem Text="Admin" Selectable="False" Value="6" >
                                <asp:MenuItem Text="Users" Value="60" ></asp:MenuItem>
                            </asp:MenuItem> 
                        </Items>
                        <StaticMenuItemStyle  CssClass="menuItem" />
                        <StaticHoverStyle BackColor="#67A5BA"/>
                        <StaticSelectedStyle BackColor="#67A5BA" />
                        <DynamicMenuItemStyle CssClass="submenuItem" BackColor="#67A5BA" VerticalPadding="3px"/>
                        <DynamicHoverStyle BackColor="#197898" />
                        <DynamicSelectedStyle BackColor="#197898" />
                        <DynamicMenuStyle CssClass="adjustedZIndex" />
                    </asp:Menu>
                </div>
            </div>              
        </div>
	    <!--header end-->