﻿
Partial Class HeaderMenu
    Inherits System.Web.UI.UserControl
    Dim TAPages As New AccessDataTableAdapters.PagesTableAdapter
    Dim DTPages As AccessData.PagesDataTable
    Dim strPageName As String
    Dim TACheckPermission As New AccessDataTableAdapters.CheckUserPagePermissionsTableAdapter
    Dim TAUsers As New AccessDataTableAdapters.UsersTableAdapter

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'hide the logout button
        Select Case Page.AppRelativeVirtualPath.ToLower
            Case "~/login.aspx", "~/error.aspx"
                btnLogout.Visible = False
            Case "~/news.aspx", "~/peeranalysis.aspx", "~/adminfeedbackdata.aspx", "~/survey.aspx"
                btnLogout.ImageUrl = "images/btnClose.jpg"
                btnLogout.Visible = True
            Case Else
                btnLogout.ImageUrl = "images/btnLogout.jpg"
                btnLogout.Visible = True
        End Select
        'If (Page.FindControl("menu1") Is Nothing) Then
        '    btnLogout.Visible = False
        'End If

        strPageName = Right(Page.AppRelativeVirtualPath, Page.AppRelativeVirtualPath.Length - 2)

        If Session("CheckPerm") = "" Then
            'either at the start of the session or after a session time-out
            're-assess all pre-conditions
            Dim TAPages As New AccessDataTableAdapters.PagesTableAdapter
            Dim DTPages As AccessData.PagesDataTable = TAPages.GetAllPages()
            For Each rowPage As AccessData.PagesRow In DTPages.Rows
                'only checks the pages which require permission
                If rowPage.checkPermission Then
                    PreCond(Page.User.Identity.Name, "", rowPage.pageID)
                End If
            Next
            Session("CheckPerm") = "Done"
        End If

        If Not IsPostBack Then
            'disables any menu item for which the user has not the permission OR the page is not yet valid
            'done in the dataset definition
            Dim DTPermission As AccessData.CheckUserPagePermissionsDataTable = TACheckPermission.GetPermissions(CInt(Page.User.Identity.Name))
            If DTPermission.Rows.Count > 0 Then
                'creates a collection of all permissions
                Dim permID(DTPermission.Rows.Count - 1) As Integer
                Dim rowPerm As AccessData.CheckUserPagePermissionsRow
                For i = 0 To DTPermission.Rows.Count - 1
                    rowPerm = DTPermission.Rows(i)
                    permID(i) = rowPerm.pageID
                Next
                filterMenuByPermission(menu1.Items, permID)
            Else
                filterMenuByPermission(menu1.Items)
            End If

            'remove the admin menu if the user is not of type Admin
            Dim TheUser As New qmulUser
            TheUser.initFromDB(Page.User.Identity.Name)
            If TheUser.uType <> qmulUser.userType.Admin Then
                If menu1.Items(menu1.Items.Count - 1).Value = "6" Then menu1.Items.RemoveAt(menu1.Items.Count - 1)
            End If

            'identifies and disable the menu item relative to the selected page
            selectedMenuItem(menu1.Items)

            'populate the header text
            If TheUser.OrganisationDesc <> "" Then
                UniPackageLabel.Text = TheUser.OrganisationDesc & " - "
            End If
            If TheUser.DepartmentDesc <> "" Then
                UniPackageLabel.Text = UniPackageLabel.Text & TheUser.DepartmentDesc & " - "
            End If
            UniPackageLabel.Text = UniPackageLabel.Text & " Free Package"
        Else
            'remove the admin menu if the user is not of type Admin
            Dim TheUser As New qmulUser
            TheUser.initFromDB(Page.User.Identity.Name)
            If TheUser.uType <> qmulUser.userType.Admin Then
                If menu1.Items(menu1.Items.Count - 1).Value = "6" Then menu1.Items.RemoveAt(menu1.Items.Count - 1)
            End If
        End If
    End Sub

    Sub filterMenuByPermission(ByVal mItems As MenuItemCollection, Optional ByVal permColl As Integer() = Nothing)
        'recursive function
        'which goes through all menuItems of the specified collection
        'disable any menuItem which value is not part of the user's permission set
        For Each mItem As MenuItem In mItems
            mItem.Enabled = grantPermission(mItem, permColl)
            If mItem.ChildItems.Count > 0 Then filterMenuByPermission(mItem.ChildItems, permColl)
        Next
    End Sub

    Function grantPermission(ByVal mItem As MenuItem, Optional ByVal permColl As Integer() = Nothing) As Boolean
        DTPages = TAPages.GetPageByID(mItem.Value)
        'first check if that item requires permission
        If DTPages.Rows.Count > 0 Then
            If Not DTPages(0).checkPermission Then
                'does not require permission
                If DTPages(0).validFrom <= Now Then
                    Return True     'and is valid
                Else
                    'BUT not yet valid
                    Dim TheUser As New qmulUser
                    TheUser.initFromDB(Page.User.Identity.Name)
                    If TheUser.uType = qmulUser.userType.Admin Then
                        Return True 'give access nevertheless to the Admin user
                    Else
                        Return False
                    End If
                End If
            End If
        Else
            'this menu item is not related to a page therefore keep it as enabled, as should be not selectable
            Return True
        End If

        'page requires permission
        If Not permColl Is Nothing Then
            'returns true if the menuItem value is part of the specified user's permission set
            For i As Integer = 0 To permColl.GetUpperBound(0)
                If mItem.Value = permColl(i).ToString Then
                    Return True
                End If
            Next
        End If
        Return False
    End Function

    Function selectedMenuItem(ByVal mItems As MenuItemCollection) As Boolean
        'recursive function
        'which goes through all menuItems of the specified collection
        'to determine if their target is the current page and therefore it needs to be flagged as selected
        'if found it bails out if not it checks the child items collection (ie the submenus)

        Dim result As Boolean = False
        For Each mItem As MenuItem In mItems
            DTPages = TAPages.GetPageByID(mItem.Value)
            If DTPages.Rows.Count > 0 Then
                If String.Compare(DTPages(0).pName, strPageName, True) = 0 Then
                    If mItem.Enabled And mItem.Selectable Then
                        'the user has permission to the current page
                        If mItem.Depth < menu1.StaticDisplayLevels + menu1.MaximumDynamicDisplayLevels Then
                            mItem.Selected = True
                            mItem.Selectable = False
                        End If
                        Return True
                    Else
                        'that page is not currently available or not selectable
                        Response.Redirect("Default.aspx")
                    End If

                End If
            End If

            If mItem.ChildItems.Count > 0 Then
                result = selectedMenuItem(mItem.ChildItems)
                If result Then
                    'flag the parent menu as selected
                    'ONE CANNOT SET IT AS SELECTED AS ONLY MENUITEM CAN BE SELECTED AT ONE TIME
                    'mItem.Text = mItem.Text & "*"
                    mItem.PopOutImageUrl = "images/menu_arrow.png"
                    Return True
                End If
            End If
        Next
        Return result
    End Function

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If Not IsPostBack Then
            'necessary to grey out the disabled items
            disableMenuItem(menu1.Items)
        End If
    End Sub

    Function disableMenuItem(ByVal mItems As MenuItemCollection) As Boolean
        'recursive function
        'which goes through all menuItems of the specified collection
        'if menu is disabled and none of its children items are enabled it greys it out
        'if a child item is enabled then only sets the top menu to non selectable
        Dim result As Boolean = False
        disableMenuItem = False
        For Each mItem As MenuItem In mItems
            result = False
            If mItem.ChildItems.Count > 0 Then
                result = disableMenuItem(mItem.ChildItems)
            End If
            If Not result And mItem.Enabled = False Then
                mItem.Text = "<span style='color:#3a768a;'>" & mItem.Text & "</span>"
                mItem.ToolTip = "Not available"
            ElseIf result And mItem.Enabled = False Then
                mItem.Enabled = True
                mItem.Selectable = False
            ElseIf mItem.Enabled = True Then
                disableMenuItem = True
            End If
        Next

    End Function

    Protected Sub menu1_MenuItemClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.MenuEventArgs) Handles menu1.MenuItemClick
        DTPages = TAPages.GetPageByID(e.Item.Value)
        If DTPages.Rows.Count > 0 Then
            'redirect to the newly selected page
            If (e.Item.Target = "_blank") Then
                Response.Write("<script>window.open('" + DTPages(0).pName + "','_blank');</script>")
            Else
                Response.Redirect(DTPages(0).pName)
            End If
        Else
            'redirect to the current one easier to reprocess menu
            Response.Redirect(strPageName)
        End If
    End Sub

    Protected Sub btnLogout_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLogout.Click
        If btnLogout.ImageUrl = "images/btnLogout.jpg" Then
            FormsAuthentication.SignOut()
            'required to make sure that if logs in as somebody else, the support user is re-processed
            Session.Abandon()
            Response.Redirect("Login.aspx")
        ElseIf btnLogout.ImageUrl = "images/btnClose.jpg" Then
            Dim strClose As String
            strClose = "<script>javascript:window.close();</script>"
            Page.Controls.Add(New LiteralControl(strClose))
        End If
    End Sub

End Class
