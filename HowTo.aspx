﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="TextPage.aspx.vb" Inherits="TextPage"%>
<%@ Register src="HeaderMenu.ascx" tagname="HeaderMenu" tagprefix="uc1" %>
<%@ Register src="footer.ascx" tagname="footer" tagprefix="uc3" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>How To</title>
    <link href="style.css" rel="stylesheet" type="text/css" />
    <link href="layout.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>       
    <script type="text/javascript">
        function setHeartbeat() {
            setTimeout("heartbeat()", 300000); // every 5 min
        }

        function heartbeat() {
            $.post("SessionHeartbeat.ashx");
            setHeartbeat();
        }
	</script>       
</head>
<body onload="setHeartbeat();">
    <form id="form1" runat="server">
    <div id="main">
	    <!--header -->
        <uc1:HeaderMenu ID="HeaderMenu1" runat="server" />
	    <!--header end-->
	    <div id="content">
		    <div class="left-top">
                <div class="right-top">
                    <div class="left-bot">
                        <div class="right-bot">
                            <div class="bg1">
                                <div class="bg2">
                            	    <div class="container">
                            	        <div class="indent">
                            	            <h1>How To Videos</h1>
                            	            Bite size videos showing you how to do common tasks in the virtual lab.<br />  
                                            <div class="column-1">
                                            <h2 >Use glassware to measure volume</h2>
                                            <iframe width="420" height="315" src="//www.youtube.com/embed/zGuWMa0MS40" frameborder="0" allowfullscreen></iframe></div>
                                            <div class="column-2">
                                            <h2 >Pick something up off the floor</h2>
                                            <iframe width="420" height="315" src="//www.youtube.com/embed/fC4xleEapng" frameborder="0" allowfullscreen></iframe></div>
                                            <div class="column-1">
                                            <h2 >Move around the lab</h2>
                                            <iframe width="420" height="315" src="//www.youtube.com/embed/T7HR_K1_ubM" frameborder="0" allowfullscreen></iframe>
                                            </div>
                                            <div class="column-2">
                                            <h2 >Label a container</h2>
                                            <iframe width="420" height="315" src="//www.youtube.com/embed/pjufJJ8pRNs" frameborder="0" allowfullscreen></iframe>
                            	            </div>
                                            <div class="column-1">
                                            <h2 >Use tall glassware</h2>
                                            <iframe width="420" height="315" src="//www.youtube.com/embed/dpbDV4MSiVQ" frameborder="0" allowfullscreen></iframe>
                                            </div>
                                            </div>
                            	    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
	    </div>
	    <!--footer -->
         <uc3:footer ID="footer1" runat="server" />
	    <!--footer end-->
	</div>	
    </form>
</body>
</html>
