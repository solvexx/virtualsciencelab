﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="TextPage.aspx.vb" Inherits="TextPage" EnableEventValidation="false"%>
<%@ Register src="HeaderMenu.ascx" tagname="HeaderMenu" tagprefix="uc1" %>
<%@ Register src="footer.ascx" tagname="footer" tagprefix="uc3" %>
<%@ Register src="EquipmentList.ascx" tagname="EquipmentList" tagprefix="uc2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Intro: Centrifuge</title>
    <link href="style.css" rel="stylesheet" type="text/css" />
    <link href="layout.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>       
    <script type="text/javascript">
        function setHeartbeat() {
            setTimeout("heartbeat()", 300000); // every 5 min
        }

        function heartbeat() {
            $.post("SessionHeartbeat.ashx");
            setHeartbeat();
        }

        function openVirtualLab(url) 
        {
            window.open(url, '_blank')
        }
	</script>       
</head>
<body onload="setHeartbeat();">
    <form id="form1" runat="server">
    <div id="main">
	    <!--header -->
        <uc1:HeaderMenu ID="HeaderMenu1" runat="server" />
	    <!--header end-->
	    <div id="content">
		    <div class="left-top">
                <div class="right-top">
                    <div class="left-bot">
                        <div class="right-bot">
                            <div class="bg1">
                                <div class="bg2">
                            	    <div class="container">
                            	        <div class="indent">
                            	            <h1>How to balance a centrifuge</h1>
                                            <div class="column-6">
                                                In this method, you will learn how to balance a centrifuge using an Eppendorf 5430 microcentrifuge.
                            	                <ol class="padding1">
                                	                <li>You will try to start the centrifuge while not balanced</li>
                                	                <li>Balance the centrifuge with 2 tubes</li>
                                	                <li>Balance the centrifuge with 3 tubes</li>
                                                    <li>Check whether you have correctly completed the method by clicking the “submit” button on the Procedure window.</li>
                                                </ol>
                                                <br />Step by step feedback is available, to view it click the “feedback” button on the Step window.
                                                <div class="indent">
                                                <asp:ImageButton ID="btnStart" runat="server" OnClientClick= "openVirtualLab('http://www.virtualsciencelab.com/Centrifuge.aspx','_blank')" ImageUrl="images/btnOpenVirtualLab.jpg" AlternateText="Start" /> 
                                                </div>
                                            </div>
                                            <div class="column-7">
                                                <div class="clear centre"><img src="images/54076.jpg" alt="Eppendorf 5430 Centrifuge" /></div>Eppendorf 5430 microcentrifuge
                                                <div class="indent centre">
                                                <asp:ImageButton ID="ImageButton1" runat="server" OnClientClick= "openVirtualLab('https://online-shop.eppendorf.co.uk/UK-en/Centrifugation-44533/Centrifuges-44534/Centrifuges-5430--5430R-PF-9159.html','_blank')" ImageUrl="images/btnMoreInfo.jpg" AlternateText="Start" />
                                                </div>
                                            </div>
                                            <uc2:EquipmentList ID="EquipmentList1" runat="server" />
                               	        </div>
                            	    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
	    </div>
	    <!--footer -->
         <uc3:footer ID="footer1" runat="server" />
	    <!--footer end-->
	</div>	
    <asp:HiddenField ID="hiddenxmlfilename" runat="server" Value="/proc_101_initial.xml" />  
    </form>
</body>
</html>
