﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="TextPage.aspx.vb" Inherits="TextPage"%>
<%@ Register src="HeaderMenu.ascx" tagname="HeaderMenu" tagprefix="uc1" %>
<%@ Register src="footer.ascx" tagname="footer" tagprefix="uc3" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Intro: Ethanoic acid based buffer</title>
    <link href="style.css" rel="stylesheet" type="text/css" />
    <link href="layout.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>       
    <script type="text/javascript">
        function setHeartbeat() {
            setTimeout("heartbeat()", 300000); // every 5 min
        }

        function heartbeat() {
            $.post("SessionHeartbeat.ashx");
            setHeartbeat();
        }

        function openVirtualLab(url) 
        {
            window.open(url, '_blank')
        }
	</script>       
</head>
<body onload="setHeartbeat();">
    <form id="form1" runat="server">
    <div id="main">
	    <!--header -->
        <uc1:HeaderMenu ID="HeaderMenu1" runat="server" />
	    <!--header end-->
	    <div id="content">
		    <div class="left-top">
                <div class="right-top">
                    <div class="left-bot">
                        <div class="right-bot">
                            <div class="bg1">
                                <div class="bg2">
                            	    <div class="container">
                            	        <div class="indent">
                            	            <h1>Ethanoic acid / ethanoate buffer solutions</h1>
                                            The object of this procedure is to measure the pH of several ethanoic acid/ethanoate buffer solutions and to determine their buffering capacity.
                            	            <ol class="padding1">
                                	            <li>You will standardize the pH meter</li>
                                	            <li>Fill a burette with standard 0.10 mol.dm<sup>-3</sup> NaOH solution</li>
                                	            <li>Fill a burette with ethanoic acid solution<sup style="color: white;">3</sup></li>
                                	            <li>Fill a burette with standard 0.10 mol.dm<sup>-3</sup> sodium ethanoate solution</li>
                                	            <li>From the burette, transfer 20 cm<sup>3</sup> of the sodium ethanoate solution provided into a 100 cm<sup>3</sup> beaker. Record the initial and final readings on the burette.</li>
                                	            <li>Add 1 cm<sup>3</sup> of the NaOH solution from the burette and record the relevant burette readings and the pH of the solution after the addition of NaOH</li>
                                	            <li>Repeat the 3 previous steps with 6 other solutions containing different amounts of ethanoic acid and sodium ethanoate<sup style="color: white;">3</sup></li>
                                	            <li>Check whether you have correctly completed the method by clicking the “submit” button on the Procedure window.<sup style="color: white;">3</sup></li>
                                            </ol>
                               	        </div>
                                        <div class="indent">
                                            <asp:ImageButton ID="btnStart" runat="server" OnClientClick= "openVirtualLab('http://www.virtualsciencelab.com/EthanoicAcidEthonoateBuffer.aspx','_blank')" ImageUrl="images/btnOpenVirtualLab.jpg" AlternateText="Start" />
                                        </div>
                            	    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
	    </div>
	    <!--footer -->
         <uc3:footer ID="footer1" runat="server" />
	    <!--footer end-->
	</div>	
    </form>
</body>
</html>
