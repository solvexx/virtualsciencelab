﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="TextPage.aspx.vb" Inherits="TextPage"%>
<%@ Register src="HeaderMenu.ascx" tagname="HeaderMenu" tagprefix="uc1" %>
<%@ Register src="footer.ascx" tagname="footer" tagprefix="uc3" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Intro: Gilson pipettes</title>
    <link href="style.css" rel="stylesheet" type="text/css" />
    <link href="layout.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>       
    <script type="text/javascript">
        function setHeartbeat() {
            setTimeout("heartbeat()", 300000); // every 5 min
        }

        function heartbeat() {
            $.post("SessionHeartbeat.ashx");
            setHeartbeat();
        }

        function openVirtualLab(url) 
        {
            window.open(url, '_blank')
        }
	</script>       
</head>
<body onload="setHeartbeat();">
    <form id="form1" runat="server">
    <div id="main">
	    <!--header -->
        <uc1:HeaderMenu ID="HeaderMenu1" runat="server" />
	    <!--header end-->
	    <div id="content">
		    <div class="left-top">
                <div class="right-top">
                    <div class="left-bot">
                        <div class="right-bot">
                            <div class="bg1">
                                <div class="bg2">
                            	    <div class="container">
                            	        <div class="indent">
                            	            <h1>How to setup 1000 µL, 200 µL and 20 µL Gilson pipettes</h1>
                                            The object of this procedure is to demonstrate how to setup Gilson pipettes of different volumes.<br />Below a certain volume the Gilson pipette cannot be used until a bigger volume is selected.<br />Beyond a certain volume the Gilson pipette will brake and can no more be used. 
                            	            <ol class="padding1">
                                	            <li>You will add a tip to the 1000 µL Gilson pipette</li>
                                	            <li>Set the value to aspirate 990µL</li>
                                	            <li>You will add a tip to the 200 µL Gilson pipette</li>
                                	            <li>Set the value to aspirate 155µL</li>
                                                <li>You will add a tip to the 20 µL Gilson pipette</li>
                                	            <li>Set the value to aspirate 15.5µL</li>
                                                <li>Check whether you have correctly completed the method by clicking the “submit” button on the Procedure window.</li>
                                            </ol>
                                            <p>Step by step feedback is available, to view it click the “feedback” button on the Step window.</p>
                               	        </div>
                                        <div class="indent">
                                            <asp:ImageButton ID="btnStart" runat="server" OnClientClick= "openVirtualLab('http://www.virtualsciencelab.com/GilsonPipettes.aspx','_blank')" ImageUrl="images/btnOpenVirtualLab.jpg" AlternateText="Start" />
                                        </div>
                            	    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
	    </div>
	    <!--footer -->
         <uc3:footer ID="footer1" runat="server" />
	    <!--footer end-->
	</div>	
    </form>
</body>
</html>
