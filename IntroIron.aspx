﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="TextPage.aspx.vb" Inherits="TextPage"%>
<%@ Register src="HeaderMenu.ascx" tagname="HeaderMenu" tagprefix="uc1" %>
<%@ Register src="footer.ascx" tagname="footer" tagprefix="uc3" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Intro: Iron absorbance</title>
    <link href="style.css" rel="stylesheet" type="text/css" />
    <link href="layout.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>       
    <script type="text/javascript">
        function setHeartbeat() {
            setTimeout("heartbeat()", 300000); // every 5 min
        }

        function heartbeat() {
            $.post("SessionHeartbeat.ashx");
            setHeartbeat();
        }

        function openVirtualLab(url) 
        {
            window.open(url, '_blank')
        }
	</script>       
</head>
<body onload="setHeartbeat();">
    <form id="form1" runat="server">
    <div id="main">
	    <!--header -->
        <uc1:HeaderMenu ID="HeaderMenu1" runat="server" />
	    <!--header end-->
	    <div id="content">
		    <div class="left-top">
                <div class="right-top">
                    <div class="left-bot">
                        <div class="right-bot">
                            <div class="bg1">
                                <div class="bg2">
                            	    <div class="container">
                            	        <div class="indent">
                            	            <h1>Determination of Iron by spectrophotometry</h1>
                                            The object of this procedure is to verify that the Beer-Lambert law holds for ferroin by measuring the absorbance of a series of solutions of known concentration using a spectrophotometer. 
                                            With these data you will then determine the concentration of iron in a solution made up from an unknown sample and hence the amount of iron in the sample.<br />
                                            <p>This procedure is split in 4 parts to avoid long sessions. You can select the different parts through the dropdown list in the procedure window. You can access the parts even if the previous was not successfully submitted.</p>
                            	            <ol class="padding1">
                                	            <li>In Part 1, you will prepare a solution with an unknown concentration of iron, made up from a sample with an unknown amount of iron for analysis.</li>
                                	            <li>In Part 2, you will prepare a standard solution of iron (II).</li>
                                	            <li>In Part 3,  you will prepare prepare six solution of Ferroin complex.</li>
                                	            <li>In Part 4, you will measure the absorbance of the different Ferroin solutions.</li>
                                	            <li>For each part, check whether you have correctly completed it by clicking the “submit” button on the Procedure window.<sup style="color: white;">3</sup></li>
                                	        </ol>
                               	        </div>
                                        <div class="indent">
                                            <asp:ImageButton ID="btnStart" runat="server" OnClientClick= "openVirtualLab('http://www.virtualsciencelab.com/Iron.aspx','_blank')" ImageUrl="images/btnOpenVirtualLab.jpg" AlternateText="Start" />
                                        </div>
                            	    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
	    </div>
	    <!--footer -->
         <uc3:footer ID="footer1" runat="server" />
	    <!--footer end-->
	</div>	
    </form>
</body>
</html>
