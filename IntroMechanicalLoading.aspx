﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="TextPage.aspx.vb" Inherits="TextPage"%>
<%@ Register src="HeaderMenu.ascx" tagname="HeaderMenu" tagprefix="uc1" %>
<%@ Register src="footer.ascx" tagname="footer" tagprefix="uc3" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Intro: Mechanical loading</title>
    <link href="style.css" rel="stylesheet" type="text/css" />
    <link href="layout.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>       
    <script type="text/javascript">
        function setHeartbeat() {
            setTimeout("heartbeat()", 300000); // every 5 min
        }

        function heartbeat() {
            $.post("SessionHeartbeat.ashx");
            setHeartbeat();
        }

        function openVirtualLab(url) 
        {
            window.open(url, '_blank')
        }
	</script>       
</head>
<body onload="setHeartbeat();">
    <form id="form1" runat="server">
    <div id="main">
	    <!--header -->
        <uc1:HeaderMenu ID="HeaderMenu1" runat="server" />
	    <!--header end-->
	    <div id="content">
		    <div class="left-top">
                <div class="right-top">
                    <div class="left-bot">
                        <div class="right-bot">
                            <div class="bg1">
                                <div class="bg2">
                            	    <div class="container">
                            	        <div class="indent">
                            	            <h1>Mechanical loading</h1>
                                            The object of this procedure is set-up and assemble the bioreactor device in order to apply dynamic compression.
                            	            <ol class="padding1">
                                	            <li>Transfer constructs to the bioreactor</li>
                                	            <li>Add culture medium</li>
                                	            <li>Program the bioreactor for dynamic compression</li>
                                	            <li>Check whether you have correctly completed the method by clicking the “submit” button on the Procedure window.<sup style="color: white;">3</sup></li>
                                            </ol>
                               	        </div>
                                        <div class="indent">
                                            <asp:ImageButton ID="btnStart" runat="server" OnClientClick= "openVirtualLab('http://www.virtualsciencelab.com/MechanicalLoading.aspx','_blank')" ImageUrl="images/btnOpenVirtualLab.jpg" AlternateText="Start" />
                                        </div>
                            	    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
	    </div>
	    <!--footer -->
         <uc3:footer ID="footer1" runat="server" />
	    <!--footer end-->
	</div>	
    </form>
</body>
</html>
