﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="IntroSpectrometer.aspx.vb" Inherits="IntroSpectrometer" EnableEventValidation="false"%>
<%@ Register src="HeaderMenu.ascx" tagname="HeaderMenu" tagprefix="uc1" %>
<%@ Register src="footer.ascx" tagname="footer" tagprefix="uc3" %>

<%@ Register src="EquipmentList.ascx" tagname="EquipmentList" tagprefix="uc2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Intro: Spectrophotometer</title>
    <link href="style.css" rel="stylesheet" type="text/css" />
    <link href="layout.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>       
    <script type="text/javascript">
        function setHeartbeat() {
            setTimeout("heartbeat()", 300000); // every 5 min
        }

        function heartbeat() {
            $.post("SessionHeartbeat.ashx");
            setHeartbeat();
        }

        function openVirtualLab(url) 
        {
            window.open(url, '_blank')
        }
	</script>       
</head>
<body onload="setHeartbeat();">
    <form id="form1" runat="server">
    <div id="main">
	    <!--header -->
        <uc1:HeaderMenu ID="HeaderMenu1" runat="server" />
	    <!--header end-->
	    <div id="content">
		    <div class="left-top">
                <div class="right-top">
                    <div class="left-bot">
                        <div class="right-bot">
                            <div class="bg1">
                                <div class="bg2">
                            	    <div class="container">
                            	        <div class="indent">
                                            <h1>How to measure absorbance</h1>
                                            <div class="column-6">
                                                The object of this procedure is to demonstrate how to use Biochrom WPA Biowave II spectrophotometer to measure absorbance.
                                                <ol class="padding1">
                                	                <li>You will start the spectrophotometer</li>
                                	                <li>Set the wavelength</li>
                                	                <li>Zero the spectrophotometer</li>
                                	                <li>Measure the absorbance</li>
                                                    <li>Check whether you have correctly completed the method by clicking the “submit” button on the Procedure window.</li>
                                                </ol>
                                                <br />Step by step feedback is available, to view it click the “feedback” button on the Step window.<br /><br />
                                                In this technique, after a successful feedback has been viewed the next step is automatically displayed.
                                                     <div class="indent">
                                                    <asp:ImageButton ID="btnStart" runat="server" OnClientClick= "openVirtualLab('http://www.virtualsciencelab.com/Spectrometer.aspx','_blank')" ImageUrl="images/btnOpenVirtualLab.jpg" AlternateText="Start" />
                                                </div>
                                            </div>
                                            <div class="column-7">
                                                <div class="clear centre"><img src="images/biowaveII.png" alt="Biochrom WPA Biowave II UV/Visible Spectrophotometer"/></div>Biochrom WPA Biowave II UV/Visible Spectrophotometer with Life Science Methods
                                                <div class="indent centre">
                                                <asp:ImageButton ID="ImageButton1" runat="server" OnClientClick= "openVirtualLab('http://www.biochrom.co.uk/product/24/biochrom-wpa-biowave-ii-uv-visible-spectrophotometer-with-life-science-methods.html','_blank')" ImageUrl="images/btnMoreInfo.jpg" AlternateText="Start" />
                                                </div>
                                            </div>
                                            <uc2:EquipmentList ID="EquipmentList1" runat="server" />  
                               	        </div>
                            	    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
	    </div>
	    <!--footer -->
         <uc3:footer ID="footer1" runat="server" />
	    <!--footer end-->
	</div>
        <asp:HiddenField ID="hiddenxmlfilename" runat="server" Value="/proc_26_initial.xml" />   	
    </form>
</body>
</html>
