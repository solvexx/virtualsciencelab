﻿

Partial Class IntroSpectrometer
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim strPageName As String = Right(Page.AppRelativeVirtualPath, Page.AppRelativeVirtualPath.Length - 2)

        'to fix menu pb in Chrome and Safari
        If Request.UserAgent.IndexOf("AppleWebKit") > 0 Then Request.Browser.Adapters.Clear()

        If Request.IsAuthenticated Then
            If Not Page.IsPostBack Then
                'process the preCondition if required and not yet done
                If PreCond(User.Identity.Name, strPageName) Then
                    Track(sender, Context, CommonUtilities.WebTrackingType.View, , User.Identity.Name)
                Else
                    'the precondition is not yet met no need to continue any further
                    'the redirection to the Home page is done by menu.ascx.vb
                End If
            End If
        Else
            Dim otherQueryString As String = ""
            If (Request.QueryString("verifcode") <> "") Then
                otherQueryString = "&verifcode=" & Request.QueryString("verifcode")
            End If
            Response.Redirect("Login.aspx?ReturnURL=" & strPageName & otherQueryString)
        End If
    End Sub

 
End Class
