﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="IntrosGAG2.aspx.vb" Inherits="sGAGInput" %>
<%@ Register src="HeaderMenu.ascx" tagname="HeaderMenu" tagprefix="uc1" %>
<%@ Register src="footer.ascx" tagname="footer" tagprefix="uc3" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Intro: sGAG assay short</title>
    <link href="style.css" rel="stylesheet" type="text/css" />
    <link href="layout.css" rel="stylesheet" type="text/css" />  
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>       
    <script type="text/javascript">
        function setHeartbeat() {
            setTimeout("heartbeat()", 300000); // every 5 min
        }

        function heartbeat() {
            $.post("SessionHeartbeat.ashx");
            setHeartbeat();
        }

        function openVirtualLab(url) {
            window.open(url, '_blank')
        }
	</script>          
</head>
<body onload="pageInit();">
    <form id="form1" runat="server" enctype="multipart/form-data" method="post">
	<div id="main">
		<!--header -->
		<uc1:HeaderMenu ID="HeaderMenu1" runat="server" />
	    <!--header end-->
		<div id="content">
			<div class="left-top">
                <div class="right-top">
                    <div class="left-bot">
                        <div class="right-bot">
                            <div class="bg1">
                                <div class="bg2">
                                	<div class="container">
                                	    <div class="indent">
                                	        <h1>sGAG assay</h1>
                                	        <h2>Aim</h2>
                                	        To determine the total amount of sulphated GAG produced by chondrocytes cultured in agarose constructs subjected to mechanical loading for 48 hours.
                                	        <div class="padding1">The digested agarose constructs will be assayed in duplicate for sGAG, using a spectrophotometric methods based 
                                	        on 1-9-dimethylmethylene blue (DMB) binding. The dye complexes with sulphated GAGs causing a metachromatic shift in the absorbance maximum from 600nm 
                                	        to 535nm</div>
                                	        <ol class="padding1">
                                	            <li>You will prepare a series of standard dilutions between 0 and 50&#181;g/mL</li>
                                	            <li>Pipette 40&#181;L of standard into 96-well plate</li>
                                	            <li>Add 40&#181;L of unknown samples  (U1-U6) n duplicate to wells</li>
                                	            <li>Add 250&#181;L DMB reagent to wells containing standard and samples</li>
                                	        </ol>
                                	        <h2 class="padding2">You need to use the following 96-well plate layout:</h2>
                                            <asp:ImageButton ID="btnDownloadLayout" runat="server" ImageUrl="images/btnDownload.jpg" AlternateText="Download" />
                                	        <img src="images/wellplate2.jpg" alt="96-well plate layout"/>
                                	        <div class="padding1">
                                	            Where:<ul>
                                                    <li><span style="background-color:#F70001; color:Black;">&nbsp;Blank&nbsp;</span> = Control 0&#181;g/mL</li>
                                                    <li><span style="background-color:#3166FF; color:Black;">&nbsp;xx&nbsp;</span> = GAG standards from 5 to 50&#181;g/mL</li>
                                                    <li><span style="background-color:#FF99CB; color:Black;">&nbsp;U1-U6&nbsp;</span> = unstrained constructs</li>
                                                    <li>Empty = no samples / empty wells</li>
                                                </ul>
                                            </div>
                                            <h2 class="padding1"><asp:label id="H2Submit" runat="server">You need to submit all 4 parts of the sGAG assay in order to gain access to the results</asp:label></h2> 
                                	        <asp:ImageButton ID="btnStart" runat="server" OnClientClick= "openVirtualLab('http://www.virtualsciencelab.com/sGAG2.aspx','_blank')" ImageUrl="images/btnOpenVirtualLab.jpg" AlternateText="Start" />
                                            <h2 id="H2Results" runat="server" class="padding1" Visible="false">You can access the results <a href="ResultsGAG2.aspx">here</a></h2> 
                                        </div>
                                	</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--footer -->
        <uc3:footer ID="footer1" runat="server" />
	    <!--footer end-->
    </div>
    <asp:Button ID="btnDwld" CausesValidation="false" runat="server" style="display:none;"  />
    <input id="DwldReady" type="hidden" runat="server" />
    <script language="JavaScript" type="text/javascript">
        function pageInit() {
            setHeartbeat();       
       
           var dwldReady = document.getElementById('DwldReady');

           if (dwldReady.value != "") {

               var button = document.getElementById('btnDwld');

               if (navigator.userAgent.match("Firefox") == "Firefox") {
                   // FireFox
                   var e = document.createEvent("MouseEvents");
                   e.initEvent("click", true, true);
                   button.dispatchEvent(e);
               }
               else {
                   // IE
                   button.click();
               }
           }
       }
    </script>    
    </form>
</body>
</html>
