﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="learnexx3DPage.aspx.vb" Inherits="learnexx3DPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Iron absorbance</title>
    <link href="style.css" rel="stylesheet" type="text/css" />
    <link href="layout.css" rel="stylesheet" type="text/css" />
		<script type='text/javascript' src='https://ssl-webplayer.unity3d.com/download_webplayer-3.x/3.0/uo/jquery.min.js'></script>
		<script type="text/javascript">
		<!--
		    var unityObjectUrl = "http://webplayer.unity3d.com/download_webplayer-3.x/3.0/uo/UnityObject2.js";
		    if (document.location.protocol == 'https:')
		        unityObjectUrl = unityObjectUrl.replace("http://", "https://ssl-");
		    document.write('<script type="text\/javascript" src="' + unityObjectUrl + '"><\/script>');
		-->
		</script>
		<script type="text/javascript">
	    <!--
		    function getEID() {
		        u.getUnity().SendMessage("Controller", "Begin", "15010");
		    }
		    function getUID() {
		        u.getUnity().SendMessage("Controller", "receiveUserIDFromWebPage", document.getElementById("hiddenUser").value);
		    }
		    function submitProcedure(arg) {
		        CallServer("submit: " + arg, this);
		    }
		    function startProcedure(arg) {
		        CallServer("procedure: " + arg, this);
		    }
		    function trackAction(arg) {
		        CallServer("action: " + arg, this);
		    }		    
		    function ReceiveServerData(arg, context) {

		    }		    	      		    
	    -->
	    </script>  
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>       
    <script type="text/javascript">
        function setHeartbeat() {
            setTimeout("heartbeat()", 300000); // every 5 min
        }

        function heartbeat() {
            $.post("SessionHeartbeat.ashx");
            setHeartbeat();
        }
	</script>   
	<style type="text/css">
		div.broken,
		div.missing {
			margin: auto;
			position: relative;
			top: 50%;
			width: 193px;
		}
		div.broken a,
		div.missing a {
			height: 63px;
			position: relative;
			top: -31px;
		}
		div.broken img,
		div.missing img {
			border-width: 0px;
		}
		div.broken {
			display: none;
		}
	</style>		     
</head>
<body onload="setHeartbeat();" onresize="resizeUnity();">
    <form id="form1" runat="server">
	<div id="main2">		
        <div id="unityPlayer">
            <div class="missing">
                <a href="http://unity3d.com/webplayer/" title="Unity Web Player. Install now!">
                    <img alt="Unity Web Player. Install now!" src="http://webplayer.unity3d.com/installation/getunity.png" width="193" height="63" />
                </a>
            </div>
            <div class="broken">
                <a href="http://unity3d.com/webplayer/" title="Unity Web Player. Install now! Restart your browser after install.">
                    <img alt="Unity Web Player. Install now! Restart your browser after install." src="http://webplayer.unity3d.com/installation/getunityrestart.png" width="193" height="63" />
                </a>
            </div>
        </div>
	</div>		
    <input id="hiddenUser" type="hidden" runat="server" />    
    </form>
</body>
<script type="text/javascript">
	<!--
    var clientWidth;
    var clientHeight;

    //this call needs to be after the "unity player" div  
    resizeUnity();

    //100% means the unity player takes the dimension of the div it is in
    var config = {
        width: "100%",
        height: "100%",
        params: { enableDebugging: "0" }

    };
    var u = new UnityObject2(config);

    jQuery(function() {

        var $missingScreen = jQuery("#unityPlayer").find(".missing");
        var $brokenScreen = jQuery("#unityPlayer").find(".broken");
        $missingScreen.hide();
        $brokenScreen.hide();

        u.observeProgress(function(progress) {
            switch (progress.pluginStatus) {
                case "broken":
                    $brokenScreen.find("a").click(function(e) {
                        e.stopPropagation();
                        e.preventDefault();
                        u.installPlugin();
                        return false;
                    });
                    $brokenScreen.show();
                    break;
                case "missing":
                    $missingScreen.find("a").click(function(e) {
                        e.stopPropagation();
                        e.preventDefault();
                        u.installPlugin();
                        return false;
                    });
                    $missingScreen.show();
                    break;
                case "installed":
                    $missingScreen.remove();
                    break;
                case "first":
                    break;
            }
        });
        u.initPlugin(jQuery("#unityPlayer")[0], "web2.unity3d");
    });

    function resizeUnity() {
        //resizes the div the unity player is in, the player itself should be set to 100%

        // WARNING NEEDS TO BE AFTER <div id="unityPlayer"> OTHERWISE DOES NOT FIND THE DIV AS NOT YET CREATED
        clientWidth = (window.innerWidth || document.body.clientWidth);
        clientHeight = (window.innerHeight || $(window).height());

        var player = document.getElementById("unityPlayer");

        if (player) {
            if (clientWidth / clientHeight < 1.56) {
                var tempDim = Math.min(clientWidth / 4, clientHeight / 3);
                tempDim = Math.floor(tempDim);
                clientWidth = tempDim * 4;
                clientHeight = tempDim * 3;
                player.style.width = clientWidth + "px";
                player.style.height = clientHeight + "px";
            } else {
                var tempDim = Math.min(clientWidth / 16, clientHeight / 9);
                tempDim = Math.floor(tempDim);
                clientWidth = tempDim * 16;
                clientHeight = tempDim * 9;
                player.style.width = clientWidth + "px";
                player.style.height = clientHeight + "px";
            }

            if (player.parentNode) {
                player.parentNode.style.width = clientWidth + "px";
            }
        }
    }	    	    
	    -->
</script>
</html>
