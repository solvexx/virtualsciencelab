﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Login.aspx.vb" Inherits="Login" %>
<%@ Register src="Header.ascx" tagname="Header" tagprefix="uc2" %>
<%@ Register src="footer.ascx" tagname="footer" tagprefix="uc3" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="style.css" rel="stylesheet" type="text/css" />
    <link href="layout.css" rel="stylesheet" type="text/css" />
</head>
<body id="login">
    <form id="form1" runat="server">
        <script type="text/javascript" src="http://www.solvexx.com/Scripts/swfobject.js"></script>
    <div id="main">
		<!--header -->
		<div id="header">
		    <div id="topBg_login">
                <div class="menuRow">
                    <uc2:Header ID="Header1" runat="server" />
                </div>
            </div>              
        </div>
		<!--header end-->
		<div id="content">
			<div class="left-top">
                <div class="right-top">
                    <div class="left-bot">
                        <div class="right-bot">
                            <div class="bg1">
                                <div class="bg2">
                                	<div class="container">
                                        <div class="indent">
                                            <div class="container"><h1>Please Login</h1></div>
                                            <div class="column-1">
                                                <div class="labelAlignment">Email: </div>
                                                <div class="labelAlignment">Password: </div>
                                                <div class="labelAlignment"><asp:CheckBox ID="RememberMe" runat="server" /></div>
                                            </div>
                                            <div class="column-2">         
                                                <div class="rowAlignment"><asp:TextBox ID="UserName" Width="250px" runat="server" ForeColor="#5c5c5c"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="reqUsername" runat="server" ControlToValidate="UserName" CssClass="errormessage" ErrorMessage="*"></asp:RequiredFieldValidator></div>
                                                <div class="rowAlignment"><asp:TextBox ID="Password" Width="250px" runat="server" TextMode="Password" ForeColor="#5c5c5c"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="reqPwd" runat="server" ControlToValidate="Password" CssClass="errormessage" ErrorMessage="*"></asp:RequiredFieldValidator></div>
                                                <div class="rowAlignment"><asp:Literal ID="Literal1" runat="server" Text="Remember Me"></asp:Literal></div>
                                            </div>
                                            <p><asp:ImageButton ID="btnLogin" runat="server"  AlternateText="Login" ImageUrl="images/btnLogin.jpg" CausesValidation="true"/></p>
                                            <p><asp:Label ID="InvalidCredentialsMessage" runat="server" CssClass="errormessage"
                                                        Visible="False"></asp:Label></p>
                                            <p><asp:LinkButton id="linkForgottenPwd" runat="server" CausesValidation="False">Forgotten your Password ?</asp:LinkButton></p>  
                                            <p>No Login? Teachers, college and university lecturers <a href="http://learnexx.com/SignUp.aspx">sign up </a>on learnexx.com</p>  
                                            <div id="forgottenPwd" visible="false">
                                            <div id="divForgottenPwd" visible="false" runat="server" style="padding-top:10px;">
                                                <div class="column-1">
                                                    <div class="labelAlignment">Please enter your email: </div>
                                                </div>
                                                <div class="column-2">
                                                    <div class="rowAlignment"><asp:TextBox ID="txtForgottenEmail" Width="250px" runat="server" ForeColor="#5c5c5c"></asp:TextBox></div>
                                                </div>
                                                <p><asp:ImageButton ID="btnForgottenPwd" runat="server"  AlternateText="Submit" ImageUrl="images/btnSendMeMyPassword.jpg" CausesValidation="false"/></p>
                                                <p><asp:Label ID="lblInvalidForgottenPwd" runat="server" CssClass="errormessage"
                                                        Visible="False"></asp:Label></p>
                                            </div>
                                        </div>                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</div>
		<!--footer -->
         <uc3:footer ID="footer1" runat="server" />
		<!--footer end-->
	</div>
    </form>
</body>
</html>
