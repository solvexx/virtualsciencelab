﻿
Partial Class Login
    Inherits System.Web.UI.Page

    Dim TAUsers As New AccessDataTableAdapters.UsersTableAdapter
    Dim DTUsers As AccessData.UsersDataTable
    Dim TASignUps As New AccessDataTableAdapters.SignUpsTableAdapter
    Dim DTSignUps As AccessData.SignUpsDataTable
    Dim cancelledAccount As String = "Your account has been cancelled/suspended. Please contact your lecturer."
    Dim contactUsEmail As String = "mailto:contact@solvexx.com&subject=Login problem"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim strVerifCode As String = Request.QueryString("verifcode")
        If strVerifCode = "" Then
            Exit Sub
        Else
            'look for the code in the db
            DTSignUps = TASignUps.GetSignUps(strVerifCode)
            If DTSignUps.Count > 0 Then
                InvalidCredentialsMessage.Text = "Your email address has been verified, we will contact you shortly with login details"
                InvalidCredentialsMessage.Visible = True
                DTSignUps(0).VerificationCode = ""
                TASignUps.Update(DTSignUps(0))
            Else
                InvalidCredentialsMessage.Text = "Your registration could not be found or you have already verified your email address. Please contact us for support"
                InvalidCredentialsMessage.Visible = True
            End If
        End If
    End Sub

    Protected Sub btnLogin_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLogin.Click

        InvalidCredentialsMessage.Visible = False
        InvalidCredentialsMessage.Text = "Your email or password is invalid. Please try again"

        Dim strUserName As String = Trim(UserName.Text)
        Dim strPwd As String = Trim(Password.Text)

        'Retrieve the Users from the DB
        DTUsers = TAUsers.GetUser(strUserName)

        For Each rowUser As AccessData.UsersRow In DTUsers
            Dim validUsername As Boolean = (String.Compare(strUserName, rowUser.uEmail, True) = 0)     'Email not case sensitive
            Dim validPassword As Boolean = (String.Compare(strPwd, rowUser.uPwd, False) = 0)        'Password CASE SENSITIVE

            If validUsername And validPassword Then
                If rowUser.uStatus <> "" Then
                    'for the moment only one status
                    InvalidCredentialsMessage.Text = cancelledAccount
                    Exit For
                End If

                Dim boolCookies As Boolean = False
                Dim ctlRememberMe As System.Web.UI.WebControls.CheckBox = FindControl("RememberMe")
                If Not ctlRememberMe Is Nothing Then boolCookies = ctlRememberMe.Checked
                FormsAuthentication.RedirectFromLoginPage(rowUser.uID, boolCookies)
                Track(sender, Context, CommonUtilities.WebTrackingType.Login, , rowUser.uID)

            End If
        Next

        ' If we reach here, the user's credentials were invalid
        InvalidCredentialsMessage.Visible = True
    End Sub

    Protected Sub linkForgottenPwd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles linkForgottenPwd.Click
        Me.InvalidCredentialsMessage.Visible = False
        Me.txtForgottenEmail.Text = Me.UserName.Text
        Me.UserName.Text = ""
        Me.divForgottenPwd.Visible = True
    End Sub

    Protected Sub btnForgottenPwd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnForgottenPwd.Click
        Me.lblInvalidForgottenPwd.Visible = False
        Me.lblInvalidForgottenPwd.Text = "Sorry we could not find a record of the email you entered, please enter another email<br/>or <A href='" & contactUsEmail & "'>send</A> us details of your full name and we will contact you."

        Dim strUserName As String = Trim(txtForgottenEmail.Text)

        'Retrieve the Users from the DB
        DTUsers = TAUsers.GetUser(strUserName)

        If DTUsers.Rows.Count = 0 Then
            'could not find the email
            Me.lblInvalidForgottenPwd.Visible = True
        Else
            'email is not a primary key so could have several row
            'take the first one
            Dim rowUser As AccessData.UsersRow = DTUsers(0)
            Dim strPwd As String = rowUser.uPwd
            Dim userID As Integer = rowUser.uID

            'check that is valid
            If rowUser.uStatus <> "" Then
                'don't send the pwd as cancelled account
                Me.lblInvalidForgottenPwd.Text = cancelledAccount

            Else
                'prepare the email
                Dim TAEmailTracking As New AccessDataTableAdapters.EmailTrackingTableAdapter
                Dim rwEmail As AccessData.EmailTrackingRow

                Dim SendingEmail As String = "contact@solvexx.com"
                Dim SendingEmailUserName As String = "contact@solvexx"
                Dim SendingEmailPassword As String = "8a5a9534"
                Dim EmailServer As String = "mail.solvexx.com"

                Dim EMailSubject As String = "Virtual Lab"
                Dim EMailBody As String = "To access the <a href='http://virtualsciencelab.com'>Virtual Lab site</a>, please login using following password (case sensitive): " & strPwd

                'use the common utilities function to send the email, which also logs the email
                rwEmail = SendEMail(strUserName, SendingEmail, EMailSubject, EMailBody, SendingEmailUserName, SendingEmailPassword, , userID, CommonUtilities.origType.Website, userID, EMailSubject, EmailServer)
                'log the email send result, return the error message in the variable passed by reference
                If Not rwEmail Is Nothing Then
                    If rwEmail.eResult = "Email Sent" Or rwEmail.eResult = "" Then
                        'successful
                        lblInvalidForgottenPwd.Text = ""
                    Else
                        lblInvalidForgottenPwd.Text = "There was a problem - the email with your password was not sent, please <A href='" & contactUsEmail & "'>contact</A> us"
                    End If
                End If

                If lblInvalidForgottenPwd.Text = "" Then
                    lblInvalidForgottenPwd.Text = "Your password was sent to your email. If you don't receive it, please check your spam folder and then <A href='" & contactUsEmail & "'>contact</A> us"
                End If
            End If

            Me.lblInvalidForgottenPwd.Visible = True

        End If

    End Sub
End Class

