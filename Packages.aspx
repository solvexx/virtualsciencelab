﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="TextPage.aspx.vb" Inherits="TextPage"%>
<%@ Register src="HeaderMenu.ascx" tagname="HeaderMenu" tagprefix="uc1" %>
<%@ Register src="footer.ascx" tagname="footer" tagprefix="uc3" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Virtual Science Lab Packages</title>
    <link href="style.css" rel="stylesheet" type="text/css" />
    <link href="layout.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>       
    <script type="text/javascript">
        function setHeartbeat() {
            setTimeout("heartbeat()", 300000); // every 5 min
        }

        function heartbeat() {
            $.post("SessionHeartbeat.ashx");
            setHeartbeat();
        }

        function openVirtualLab(url) 
        {
            window.open(url, '_blank')
        }
	</script>       
</head>
<body onload="setHeartbeat();">
    <form id="form1" runat="server">
    <div id="main">
	    <!--header -->
        <uc1:HeaderMenu ID="HeaderMenu1" runat="server" />
	    <!--header end-->
	    <div id="content">
		    <div class="left-top">
                <div class="right-top">
                    <div class="left-bot">
                        <div class="right-bot">
                            <div class="bg1">
                                <div class="bg2">
                            	    <div class="container">
                            	        <div class="indent">
                            	            <h1>Schools, Colleges and Universities</h1>
                                            <div class="column-4">
                                            <h2 >Do you need other techniques?</h2>
                                            If you can’t find the technique you are looking for, next time you order equipment or consumables from your lab supplier ask them to sponsor a virtual science lab technique based around their equipment.
                                            </div>
                                            <div class="column-5">
                                            <h2 >Earn money from your lab methods</h2>
                                            Fund the creation of the virtual science lab method you want to use and we will pay you 10% commission whenever anyone else buys it.
                                            </div>
                                            <h2 class="clear">Packages</h2>
                                            <img src="images/PriceTable.jpg" alt="Virtual Science Lab Packages"/>
                                            <ul class="circle padding1">
                                	            <li>All students must be registered on a course provided by the organisation.</li>
                                	            <li>Learning Management System must require user authentication and techniques and methods not to be publically available.</li>
                                	            <li>Only one free offer may be redeemed per school or per college/university department.</li>
                                                <li>All prices plus VAT.</li>
                                             </ul>
                                            <p>To order a package call +44(0)845 371 6132 or <a href="mailto:contact@solvexx.com?Subject=Virtual Science Lab Packages" target="_top">email us</a></p>
                               	        </div>
                            	    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
	    </div>
	    <!--footer -->
         <uc3:footer ID="footer1" runat="server" />
	    <!--footer end-->
	</div>	
    </form>
</body>
</html>
