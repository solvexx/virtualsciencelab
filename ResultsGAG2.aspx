﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ResultsGAG2.aspx.vb" Inherits="sGAGResults" %>
<%@ Register src="HeaderMenu.ascx" tagname="HeaderMenu" tagprefix="uc1" %>
<%@ Register src="footer.ascx" tagname="footer" tagprefix="uc3" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>sGAG assay short results</title>
    <link href="style.css" rel="stylesheet" type="text/css" />
    <link href="layout.css" rel="stylesheet" type="text/css" />  
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>       
    <script type="text/javascript">
        function setHeartbeat() {
            setTimeout("heartbeat()", 300000); // every 5 min
        }

        function heartbeat() {
            $.post("SessionHeartbeat.ashx");
            setHeartbeat();
        }
	</script>          
</head>
<body onload="pageInit();">
    <form id="form1" runat="server" enctype="multipart/form-data" method="post">
	<div id="main">
		<!--header -->
		<uc1:HeaderMenu ID="HeaderMenu1" runat="server" />
	    <!--header end-->
		<div id="content">
			<div class="left-top">
                <div class="right-top">
                    <div class="left-bot">
                        <div class="right-bot">
                            <div class="bg1">
                                <div class="bg2">
                                	<div class="container">
                                	    <div class="indent">
                                	        <h1>sGAG assay</h1>
                                	        <h2>Your samples are now ready to be analysed</h2>
                                	        You should download the results from the virtual lab into Excel. The data includes absorbance values for standards and unknown samples.
                                	        <p><asp:ImageButton ID="btnDownloadResults" runat="server" ImageUrl="images/btnDownload.jpg" AlternateText="Download" /></p>
                                            <p><asp:Label ID="DownloadSuccessMessage" runat="server" Visible="False"></asp:Label><asp:Label ID="ErrorDownloadMessage" runat="server" CssClass="errormessage" Visible="False">Please download the results.</asp:Label></p>
                                	        <ol>
                                	            <li>Plot a graph of absorbance against the mean values for each standard (0 to 50&#181;g/mL).</li>
                                	            <li>Fit a linear model to the data and calculate the R value </li>
                                	        </ol>
                                	        <div class="padding1">Here is a typical GAG standard curve with a -0.96 correlation coefficient</div>
                                	        <img src="images/GAGcurve.jpg" alt="GAG standard curve"/>
                                	        <div class="padding1">Bold line represents linear trend-line. Error bars represent SD for two replicates.</div>
                                	    </div>
                                	</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--footer -->
        <uc3:footer ID="footer1" runat="server" />
	    <!--footer end-->
    </div>
    <asp:Button ID="btnDwld" CausesValidation="false" runat="server" style="display:none;"  />
    <input id="DwldReady" type="hidden" runat="server" />
    <script language="JavaScript" type="text/javascript">
        function pageInit() {
            setHeartbeat();       
       
           var dwldReady = document.getElementById('DwldReady');

           if (dwldReady.value != "") {

               var button = document.getElementById('btnDwld');

               if (navigator.userAgent.match("Firefox") == "Firefox") {
                   // FireFox
                   var e = document.createEvent("MouseEvents");
                   e.initEvent("click", true, true);
                   button.dispatchEvent(e);
               }
               else {
                   // IE
                   button.click();
               }
           }
       }
    </script>    
    </form>
</body>
</html>
