﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Error.aspx.vb" Inherits="_Error" %>
<%@ Register src="Header.ascx" tagname="Header" tagprefix="uc2" %>
<%@ Register src="footer.ascx" tagname="footer" tagprefix="uc3" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Error</title>
    <link href="style.css" rel="stylesheet" type="text/css" />
    <link href="layout.css" rel="stylesheet" type="text/css" />    
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>       
    <script type="text/javascript">
        function setHeartbeat() {
            setTimeout("heartbeat()", 300000); // every 5 min
        }

        function heartbeat() {
            $.post("SessionHeartbeat.ashx");
            setHeartbeat();
        }
	</script>       
</head>
<body id="error" onload="setHeartbeat();">
    <form id="form1" runat="server">
	<div id="main">
		<!--header -->
		<div id="header">
		    <div id="topBg">
                <div class="menuRow">
                    <uc2:Header ID="Header1" runat="server" />
                </div>
            </div>              
        </div>
		<!--header end-->
		<div id="content">
			<div class="left-top">
                <div class="right-top">
                    <div class="left-bot">
                        <div class="right-bot">
                            <div class="bg1">
                                <div class="bg2">
                                	<div class="container">
                                        <h1>Sorry, an error occured.</h1>
                                        <p>This could be due to a session time-out.</p>
                                        <p><asp:ImageButton ID="btnResume" runat="server" ImageUrl="~/images/btnResume.jpg" AlternateText="Resume" /></p>
                                        <p></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		<!--footer -->
         <uc3:footer ID="footer1" runat="server" />
		<!--footer end-->
    </div>
    </form>
</body>
</html>
