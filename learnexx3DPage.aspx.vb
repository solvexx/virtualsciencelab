﻿Imports System.IO
Imports System.Data
Imports System.Web

Partial Class learnexx3DPage
    Inherits System.Web.UI.Page
    Implements System.Web.UI.ICallbackEventHandler

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim strPageName As String = Right(Page.AppRelativeVirtualPath, Page.AppRelativeVirtualPath.Length - 2)

        'to fix menu pb in Chrome and Safari
        If Request.UserAgent.IndexOf("AppleWebKit") > 0 Then Request.Browser.Adapters.Clear()

        If Request.IsAuthenticated Then
            'process the preCondition if required and not yet done
            If PreCond(User.Identity.Name, strPageName) Then
                'the precondition is met therefore can load the embed
                'retrieves the Unity procedure
                Dim embedKey As String = ""
                Dim TAPages As New AccessDataTableAdapters.PagesTableAdapter
                Dim DTPages As AccessData.PagesDataTable = TAPages.GetPageByName(strPageName)
                If DTPages.Rows.Count > 0 Then
                    embedKey = DTPages(0).learnexx3DKey
                End If
                'embed key is statically defined in the page scripts so not used any more
                hiddenUser.Value = User.Identity.Name '& "_" & embedKey

                Track(sender, Context, CommonUtilities.WebTrackingType.View, , User.Identity.Name)

                Dim cm As ClientScriptManager = Page.ClientScript
                Dim cbReference As String
                cbReference = cm.GetCallbackEventReference(Me, "arg", "ReceiveServerData", "")
                Dim callbackScript As String = ""
                callbackScript &= "function CallServer(arg, context)" & "{" & cbReference & "; }"
                cm.RegisterClientScriptBlock(Me.GetType(), "CallServer", callbackScript, True)
            Else
                'the precondition is not yet met no need to continue any further
                'the redirection to the Home page is done by menu.ascx.vb
            End If
        Else
            Response.Redirect("Login.aspx?ReturnURL=" & strPageName)
        End If

    End Sub

    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) _
    Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim TATracking As New AccessDataTableAdapters.UserTrackingTableAdapter
        Dim DTTracking As AccessData.UserTrackingDataTable

        'as the same server sub is used by all functions called by Unity for tracking purposes
        'the javascript adds a prefix to allow identifying which tracking needs to be done
        If eventArgument.StartsWith("language: ") Then
            'the user has changed the localisation in Unity
            'store the new settings in the DB
            'NOT IMPLEMENTED
            Exit Sub
        End If
        If eventArgument.StartsWith("procedure: ") Then
            'the user has started/restarted a procedure, which unique ID is passed as input argument
            eventArgument = Right(eventArgument, Len(eventArgument) - Len("procedure: "))
            TATracking.Insert(User.Identity.Name, CInt(eventArgument), "Start")
            Exit Sub
        End If
        If eventArgument.StartsWith("action: ") Then
            'the user has triggered an action in the model
            'Unity identifies the selected eqpt by name than the button name (eg Assemble) and finally the interacted eqpt by name (empty string = no interaction)
            eventArgument = Right(eventArgument, Len(eventArgument) - Len("action: "))
            'as no post back has occured one needs to retrieve the procedure currently running
            'cannot be stored on the server but needs to be retrieved from the DB
            'GetLastTrackingByUser returns all tracking information for the specified user with the last occured record first
            DTTracking = TATracking.GetLastTrackingByUser(User.Identity.Name)
            If DTTracking.Rows.Count > 0 Then TATracking.Insert(User.Identity.Name, DTTracking(0).procId, eventArgument)
            Exit Sub
        End If
        If eventArgument.StartsWith("submit: ") Then
            'the user has submitted the procedure and the result is passed as input argument
            'as no post back has occured one needs to retrieve the procedure currently running
            'cannot be stored on the server but needs to be retrieved from the DB
            'GetLastTrackingByUser returns all tracking information for the specified user with the last occured record first
            DTTracking = TATracking.GetLastTrackingByUser(User.Identity.Name)
            If DTTracking.Rows.Count > 0 Then
                TATracking.Insert(User.Identity.Name, DTTracking(0).procId, eventArgument)

                'FOR THE MOMENT GRANT ACCESS TO NEXT PAGE IN CASE OF ALL LEARNEXX 3D pages irrespective if successful or not
                Dim TAPagePreCond As New AccessDataTableAdapters.PagePreConditionsTableAdapter
                Dim DTPagePreCond As AccessData.PagePreConditionsDataTable = TAPagePreCond.GetPagesPreCondAfterSubmit(DTTracking(0).procId)

                For Each rowPreCond As AccessData.PagePreConditionsRow In DTPagePreCond.Rows
                    PreCond(User.Identity.Name, , rowPreCond.pageID)
                Next
            End If
            
            Exit Sub
        End If

    End Sub

    Public Function GetCallbackResult() As String Implements _
    System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return ""
    End Function

End Class
